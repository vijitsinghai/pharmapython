from django.contrib import admin
from .models import Retailer, AuthToken
# Register your models here.
admin.site.register(Retailer)
admin.site.register(AuthToken)
