from django.db import models
from retailers.models import Retailer
# Create your models here.


class Customer(models.Model):
    name = models.CharField(max_length=200)
    mobile = models.CharField(max_length=200, null=False,
                              blank=True)
    email = models.CharField(max_length=200, null=False,
                             blank=True)
    patientType = models.CharField(max_length=200, null=False,
                                   blank=True)
    loyaltyDiscount = models.CharField(max_length=200, null=False,
                                       blank=True)
    addressTitle = models.CharField(max_length=200, null=False,
                                    blank=True)
    address = models.CharField(max_length=200, null=False,
                               blank=True)
    creditbalance = models.CharField(max_length=200, null=False,
                                     blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer, related_name='retailer', on_delete=models.CASCADE)


    def __int__(self):
        return self.id
