from django.db import models
from retailers.models import Retailer
# Create your models here.


class Vendor(models.Model):
    name = models.CharField(max_length=200)
    mobile = models.CharField(max_length=200, null=False,
                              blank=True)
    email = models.CharField(max_length=200, null=False,
                             blank=True)
    addresstitle = models.CharField(max_length=200, null=False,
                                    blank=True)
    address = models.CharField(max_length=200, null=False,
                               blank=True)
    creditbalance = models.CharField(max_length=200, null=False,
                                     blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer, on_delete=models.CASCADE)

    def __int__(self):
        return self.id
