from rest_framework import serializers
from .models import Products, Batch, Substitute, ProductCompany, Salt, SaltProductMapping, Unit, RetailerProductMapping

class BatchSerializer(serializers.ModelSerializer):

    class Meta:
        model = Batch
        fields = ('id', 'productid', 'number', 'expiry',
                  'mrp', 'discount', 'currentstock','retailerid')


class CompanySerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductCompany
        fields = ('id', 'name')


class UnitSerializer(serializers.ModelSerializer):

    class Meta:
        model = Unit
        fields = ('id', 'name')


class SaltSerializer(serializers.ModelSerializer):

    class Meta:
        model = Salt
        fields = ('id', 'name', 'theraputic')


class SaltMappingSerializer(serializers.ModelSerializer):

    saltdata = SaltSerializer(source="saltid", read_only=True)
    saltid = serializers.PrimaryKeyRelatedField(
        many=False,
        queryset=Salt.objects.all()
    )
    productid = serializers.PrimaryKeyRelatedField(
        many=False,
        queryset=Products.objects.all()
    )

    class Meta:
        model = SaltProductMapping
        fields = ('id', 'saltdata',  'saltid', 'productid')


class SubstituteMappingSerializer(serializers.ModelSerializer):

    substitute = serializers.CharField(
        source='substituteid', read_only=True)

    class Meta:
        model = Substitute
        fields = ('substituteid', 'substitute')


class RetilerProductMappingSerializer(serializers.ModelSerializer):

    class Meta:
        model = RetailerProductMapping
        fields = ('id', 'retailerid', 'productid', 'currentstock','rack')



class ProductsSerializer(serializers.ModelSerializer):
    company_name = serializers.CharField(source='company', read_only=True)
    unit_name = serializers.CharField(source='unit', read_only=True)
    salt_product = SaltMappingSerializer(many=True,  read_only=True)
    batch = serializers.SerializerMethodField()
    def get_batch(self, obj):
        if(self.context):
            loggedRetailer = self.context['loggedRetailer']
            return BatchSerializer(Batch.objects.filter(retailerid=loggedRetailer).filter(productid=obj.id), many=True).data
        else:
            return BatchSerializer(Batch.objects.filter(productid=obj.id), many=True).data
    company = serializers.PrimaryKeyRelatedField(
        many=False,
        queryset=ProductCompany.objects.all()
    )
    unit = serializers.PrimaryKeyRelatedField(
        many=False,
        queryset=Unit.objects.all()
    )
    retailerStock = serializers.SerializerMethodField('get_retailerstock')
    def get_retailerstock(self, obj):
        if(self.context):
            loggedRetailer = self.context['loggedRetailer']
            stock = RetilerProductMappingSerializer(RetailerProductMapping.objects.filter(
                retailerid=loggedRetailer).filter(productid=obj.id),many=True).data
            return stock
        else:
            stock = RetilerProductMappingSerializer(RetailerProductMapping.objects.filter(productid=obj.id), many=True).data
            return stock

    class Meta:
        model = Products
        fields = ('id', 'name', 'rack', 'opening_stock',
                  'company_name', 'unit_name', 'packaging', 'salt_product', 'batch', 'current_stock', 'primary_unit', 'secondary_unit', 'company', 'unit', 'narcotic', 'opening_stock', 'hsn', 'tax', 'conversion_ratio', 'retailerStock')


class SubstituteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Substitute
        fields = ('id', 'substituteid', 'productid')


class ProductsFilterSerializer(serializers.ModelSerializer):

    
    batch = BatchSerializer(many=True, read_only=True)

    class Meta:
        model = Products
        fields = ('id', 'productName', 'openingStock',
                  'rack', 'price', 'batch',  'primaryUnit', 'secondaryUnit', 'discount', 'salt', 'theraputic')
