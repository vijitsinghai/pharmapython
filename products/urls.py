from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'products'
urlpatterns = [
    path('list/', views.datalist,
         name='list'),
    path('suggestions/', views.suggestions,
         name='suggestions'),

    path('save/', views.savedata,
         name='save'),
    path('substitutes/', views.getSubstitutes,
         name='substitutes'),
    path('saltlist/', views.saltlist,
         name='saltlist'),
    path('companies/', views.companies,
         name='companies'),
    path('units/', views.units,
         name='units'),
    path('productStock/', views.productStock,
         name='productStock'),
         

    

]
