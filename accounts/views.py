import json
from django.http import JsonResponse
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import AccountGroup, Ledger, VoucherDetail,Voucher,Settings
from .serializers import AccountGroupSerializer, LedgerSerializer, VoucherSerializer, VoucherDetailSerializer
from rest_framework import status
from django.db.models import Sum
from django.db.models import F
from retailers.views import checkAuth
from customers.models import Customer
from vendors.models import Vendor   

@api_view(['GET', 'POST'])
def accountgroups(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    #account_groups = AccountGroup.objects.get(id=1)    
    accountgroups = AccountGroup.objects.filter(retailerid=loggedRetailer).filter(parent='')
    accounts = AccountGroupSerializer(accountgroups,many=True)
    return Response(accounts.data)


@api_view(['GET', 'POST'])
def childList(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    dbid = request.data.get('id')
    finaldata = []
    accountgroups = AccountGroup.objects.filter(
        retailerid=loggedRetailer).filter(parent=dbid)
    accounts = AccountGroupSerializer(accountgroups, many=True)
    finaldata.append(accounts.data)

    accountgroups = Ledger.objects.filter(
        retailerid=loggedRetailer).filter(accountgroupid=dbid)
    accounts = LedgerSerializer(accountgroups, many=True)
    finaldata.append(accounts.data)

    return Response(finaldata)


@api_view(['GET', 'POST'])
def ledgerList(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    dbid = request.data.get('id')
    accountgroups = Ledger.objects.filter(
        retailerid=loggedRetailer).filter(accountgroupid=dbid)
    accounts = LedgerSerializer(accountgroups, many=True)
    return Response(accounts.data)


@api_view(['GET', 'POST'])
def voucherlist(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    dbid = request.data.get('ledgerid')
    limit = request.data.get('limit')
    if(dbid == "0"):
        accountgroups = VoucherDetail.objects.filter(retailerid=loggedRetailer).order_by('-createdate')
    elif limit == 'all':
        accountgroups = VoucherDetail.objects.filter(retailerid=loggedRetailer).filter(ledgerid=dbid).order_by('-createdate')
    else:
        accountgroups = VoucherDetail.objects.filter(retailerid=loggedRetailer).filter(
            ledgerid=dbid).order_by('-createdate')[:5]
    accounts = VoucherDetailSerializer(accountgroups, many=True)
    return Response(accounts.data)


def saveVoucher(vouchertype, vouchersubtype, vouchertitle, ledgertypes, ledgerids, debitamounts, creditamounts, retailerid, notes, entrytypes, entryids, ledgertypeids):
    thisdata = {
        'vouchertype' : vouchertype,
        'vouchersubtype': vouchersubtype,
        'vouchertitle': vouchertitle,
        'retailerid': retailerid
    }
    vouchSer = VoucherSerializer(data=thisdata)
    if vouchSer.is_valid():
        voucherInstance = vouchSer.save()
        voucherid = voucherInstance.id
        vouchernumber = "VCH-"+str(voucherid)
        thisVoucher = Voucher.objects.get(id=voucherid)
        thisVoucher.vouchernumber = vouchernumber
        thisVoucher.save()
        voucherdata = []
        voucherdata.append(voucherid)
        voucherdata.append(vouchernumber)
        p=0
        for x in ledgertypes:
            thisData = {
                'ledgertype': ledgertypes[p],
                'ledgerid': ledgerids[p],
                'ledgertypeid': ledgertypeids[p],
                'debit': debitamounts[p],
                'credit': creditamounts[p],
                'notes' : notes[p],
                'voucherid': voucherid,
                'entrytype': entrytypes[p],
                'entryid': entryids[p],
                'retailerid' : retailerid,
                'vouchernumber':vouchernumber,
                'vouchertitle':vouchertitle
            }
            saveDetails = VoucherDetailSerializer(data=thisData)
            if saveDetails.is_valid():
                saveDetails.save()
                p = p+1
        return voucherdata
    else:
        return False


def addCustomerLedger(name,dbid,retailerid):
    thisData = {
        'name':name,
        'ledgertype' : 'Customer',
        'ledgertypeid' : dbid,
        'retailerid':retailerid,
        'accountgroupid':9
    }
    pushdata = LedgerSerializer(data=thisData)
    if pushdata.is_valid():
        pushdata.save()
        return True
    else:
        return False


def addVendorLedger(name, dbid, retailerid):
    thisData = {
        'name': name,
        'ledgertype': 'Supplier',
        'ledgertypeid': dbid,
        'retailerid': retailerid,
        'accountgroupid': 8
    }
    pushdata = LedgerSerializer(data=thisData)
    if pushdata.is_valid():
        pushdata.save()
        return True
    else:
        return False


def fixCustomerBalance(customerid):
    ledgerInstance = Ledger.objects.filter(ledgertype='Customer').get(ledgertypeid=customerid)
    ledgerid = ledgerInstance.id
    debit_sum = VoucherDetail.objects.filter(ledgerid=ledgerid).filter(
        ledgertypeid=customerid).aggregate(Sum('debit'))
    credit_sum = VoucherDetail.objects.filter(ledgerid=ledgerid).filter(
        ledgertypeid=customerid).aggregate(Sum('credit'))
    if debit_sum['debit__sum'] is None:
        debit_sum['debit__sum'] = 0
    if credit_sum['credit__sum'] is None:
        credit_sum['credit__sum'] = 0
    diff = credit_sum['credit__sum'] - debit_sum['debit__sum']
    customerInstance= Customer.objects.get(id=customerid)
    customerInstance.creditbalance = diff
    customerInstance.save()

    ledgerInstance.debit = debit_sum['debit__sum']
    ledgerInstance.credit = credit_sum['credit__sum']
    ledgerInstance.balance = diff
    ledgerInstance.save()

    return True


def fixLedgerBalance(ledgerid, ledgertype, ledgertypeid):
    updateLedgerBalance(ledgerid, ledgertype, ledgertypeid)
    updateLedgerBalance(8, '','')
    updateLedgerBalance(13, '','')
    updateLedgerBalance(14, '','')
    updateLedgerBalance(15, '', '')


def updateLedgerBalance(ledgerid,ledgertype, ledgertypeid):
    if ledgerid == 0:
        ledgerInstance = Ledger.objects.filter(
            ledgertype=ledgertype).get(ledgertypeid=ledgertypeid)
        ledgerid = ledgerInstance.id
    else:
        ledgerInstance = Ledger.objects.get(id=ledgerid)
    
    debit_sum = VoucherDetail.objects.filter(ledgerid=ledgerid).filter(
        ledgertypeid=ledgertypeid).aggregate(Sum('debit'))
    credit_sum = VoucherDetail.objects.filter(ledgerid=ledgerid).filter(
        ledgertypeid=ledgertypeid).aggregate(Sum('credit'))
    if debit_sum['debit__sum'] is None:
        debit_sum['debit__sum'] = 0
    if credit_sum['credit__sum'] is None:
        credit_sum['credit__sum'] = 0
    diff = credit_sum['credit__sum'] - debit_sum['debit__sum']

    if ledgertype == 'Customer':
        typeInstance = Customer.objects.get(id=ledgertypeid)
        typeInstance.creditbalance = diff
        typeInstance.save()

    if ledgertype == 'Supplier':
        typeInstance = Vendor.objects.get(id=ledgertypeid)
        typeInstance.creditbalance = diff
        typeInstance.save()


    ledgerInstance.debit = debit_sum['debit__sum']
    ledgerInstance.credit = credit_sum['credit__sum']
    ledgerInstance.balance = diff
    ledgerInstance.save()

    return True


@api_view(['GET', 'POST'])
def voucherdetails(request):
    finaldata = []
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    voucherid = request.data.get('voucherid')
    accountgroups = VoucherDetail.objects.filter(voucherid=voucherid)
    accounts = VoucherDetailSerializer(accountgroups, many=True)
    for x in accounts.data:
        ledgerid = x['ledgerid']
        ledgerData = Ledger.objects.filter(id=ledgerid)
        ledgerSer = LedgerSerializer(ledgerData,many=True)
        x['ledgerdata'] = ledgerSer.data
        finaldata.append(x)
    return Response(finaldata)


def getSetup(retailerid,subject):
    setupData = Settings.objects.filter(retailerid=retailerid).get(subject=subject)
    return setupData.value



