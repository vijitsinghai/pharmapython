from rest_framework import serializers
from products.serializers import ProductsSerializer
from .models import Invoice, InvoiceDetail, SalesOrder, SalesOrderDetails


class InvoiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Invoice
        fields = ('id', 'subtotal', 'discount', 'delivery_charge', 'payment_due', 'previous_credit',
                  'outstanding', 'total_invoice_value', 'invoice_date', 'prescription_days', 'payment_made', 'customerid', 'invoice_number', 'retailerid','payment_made_cash','payment_made_bank','voucherid','vouchernumber')


class InvoiceDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvoiceDetail
        fields = ('id', 'invoiceid', 'productid', 'batchid', 'mrp', 'primary_unit',
                  'secondary_unit', 'discount', 'subtotal', 'retailerid', 'stockvalue')


class SalesOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = SalesOrder
        fields = ('id', 'prepayment', 'customerid', 'createdate','retailerid')


class SalesOrderDetailSerializer(serializers.ModelSerializer):
    """    
            product_data = serializers.SerializerMethodField('get_products')
            def get_products(self, obj):
            loggedRetailer = self.context['loggedRetailer']
            products = Products.objects.filter(id=obj.productid)
            return ProductsSerializer(products,read_only=True, context={
                'loggedRetailer': loggedRetailer}).data
    """

    
    product_data = ProductsSerializer(source="productid", read_only=True)

    class Meta:
        model = SalesOrderDetails
        fields = ('id', 'salesorderid', 'productid',  'mrp', 'primary_unit',
                  'secondary_unit', 'customerid', 'total_units', 'total_units_delivered', 'product_data','retailerid')
