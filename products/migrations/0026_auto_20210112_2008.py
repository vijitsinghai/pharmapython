# Generated by Django 3.1.4 on 2021-01-12 20:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0025_auto_20210112_2007'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='narcotic',
            field=models.BooleanField(null=True),
        ),
    ]
