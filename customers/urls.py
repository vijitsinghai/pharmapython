from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'customers'
urlpatterns = [
    path('detailsviamobile/', views.detailsviamobile,
         name='detailsviamobile'),
    path('save/', views.savedata,
         name='save'),
    path('details/', views.customerdetails,
         name='details'),
    path('list/', views.list,
         name='list'),
    path('suggestions/', views.suggestions,
         name='suggestions')

]
