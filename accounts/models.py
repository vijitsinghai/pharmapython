from django.db import models
from retailers.models import Retailer
# Create your models here.


class Voucher(models.Model):
    vouchertype = models.CharField(max_length=200)
    vouchersubtype = models.CharField(max_length=200, null=False,
                              blank=True)
    vouchertitle = models.CharField(max_length=200, null=False,
                                    blank=True)
    vouchernumber = models.CharField(max_length=200, null=False,
                                    blank=True)

    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer, on_delete=models.CASCADE)

    def __int__(self):
        return self.id

class VoucherDetail(models.Model):
    voucherid = models.ForeignKey(
        Voucher, on_delete=models.CASCADE)
    ledgertype = models.CharField(max_length=200, null=False,
                                    blank=True)
    ledgerid = models.CharField(max_length=200,  null=False,
                                blank=True)
    ledgertypeid = models.CharField(max_length=200, null=False,
                                blank=True)
    debit = models.CharField(max_length=200, null=False,
                                    blank=True)
    credit = models.CharField(max_length=200, null=False,
                                    blank=True)
    notes = models.CharField(max_length=200, null=False,
                             blank=True)
    createdate = models.DateTimeField(null=False,
                                    blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer, on_delete=models.CASCADE)
    entrytype = models.CharField(max_length=200, null=False,
                             blank=True)
    entryid = models.CharField(max_length=200, null=False,
                             blank=True)
    vouchertitle = models.CharField(max_length=200, null=False,
                                    blank=True)
    vouchernumber = models.CharField(max_length=200, null=False,
                                     blank=True)


def __int__(self):
    return self.id





class AccountGroup(models.Model):
    accountname = models.CharField(max_length=200)
    parent = models.CharField(max_length=200, null=False,
                                  blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(Retailer,  on_delete=models.CASCADE)
    def __int__(self):
        return self.id


class Settings(models.Model):
    subject = models.CharField(max_length=200, null=False,
                               blank=True)
    value = models.CharField(max_length=200, null=False,
                             blank=True)
    inputtype = models.CharField(max_length=200, null=False,
                                 blank=True)
    displayname = models.CharField(max_length=200, null=False,
                              blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(Retailer,  on_delete=models.CASCADE)

    def __int__(self):
        return self.id



class Ledger(models.Model):
    accountgroupid = models.ForeignKey(AccountGroup,  on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    ledgertype = models.CharField(max_length=200, null=False,
                                  blank=True)
    ledgertypeid = models.CharField(max_length=200, null=False,
                                blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer,  on_delete=models.CASCADE)
    debit = models.CharField(max_length=200, null=False,
                                blank=True)
    credit = models.CharField(max_length=200, null=False,
                                blank=True)

    balance = models.CharField(max_length=200, null=False,
                              blank=True)

    


    def __int__(self):
        return self.id

