from rest_framework import serializers
from .models import AccountGroup, Ledger, Voucher, VoucherDetail


class AccountGroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = AccountGroup
        fields = ('id', 'accountname', 'parent', 'retailerid')


class LedgerSerializer(serializers.ModelSerializer):
    ## some change
    class Meta:
        model = Ledger
        fields = ('id', 'accountgroupid', 'name', 'ledgertype',
                  'ledgertypeid', 'retailerid','debit','credit','balance')


class VoucherSerializer(serializers.ModelSerializer):

    class Meta:
        model = Voucher
        fields = ('id', 'vouchertype', 'vouchersubtype','vouchertitle', 'retailerid','vouchernumber')


class VoucherDetailSerializer(serializers.ModelSerializer):


    class Meta:
        model = VoucherDetail
        fields = ('id', 'voucherid', 'ledgerid',
                  'ledgertype', 'debit', 'credit', 'retailerid', 'notes', 'ledgertypeid', 'entrytype', 'entryid', 'vouchernumber', 'vouchertitle', 'createdate')
