import json
from django.http import JsonResponse
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Customer
from .serializers import CustomerSerializer
from rest_framework import status
from invoices.models import Invoice, SalesOrderDetails
from invoices.serializers import InvoiceSerializer, SalesOrderDetailSerializer
from django.db.models import Sum
from django.db.models import F
from retailers.views import checkAuth
from accounts.views import addCustomerLedger
### For commit

@api_view(['GET', 'POST'])
def detailsviamobile(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    finaldata = []
    customers = Customer.objects.filter(retailerid=loggedRetailer).filter(
        mobile=request.data.get('mobile'))
    if customers:
        serializer = CustomerSerializer(customers,many=True)
    #    return Response(serializer.data)
        customerid = serializer.data[0]['id']
        finaldata.append(serializer.data)
        salesOrderDetails = SalesOrderDetails.objects.filter(
            customerid=customerid).filter(retailerid=loggedRetailer).filter(total_units_delivered__lt=F('total_units'))
        salesOrderSerData = SalesOrderDetailSerializer(
            salesOrderDetails, many=True, context={'loggedRetailer': loggedRetailer})
        finaldata.append(salesOrderSerData.data)
        return Response(finaldata)
    else:
        return HttpResponse('No Data Found')


@api_view(['GET', 'POST'])
def savedata(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')

    data = request.data.getlist('data[]')
    customerid = data[7]
    if customerid == '':
        customerid = '0'

    customerdata = {
        'name': data[0],
        'mobile': data[1],
        'email': data[2],
        'patientType': data[3],
        'loyaltyDiscount': data[4],
        'addressTitle': data[5],
        'address': data[6]
    }
    if customerid == '0':
        serializer = CustomerSerializer(data=customerdata)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    else:
        customer = Customer.objects.get(id=customerid)
        serializer = CustomerSerializer(customer, data=customerdata)
        if serializer.is_valid():
            custInstance = serializer.save()
            addCustomerLedger(data[0], custInstance.id, loggedRetailer)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def customerdetails(request):
    if request.data.get('id'):
        customerid = request.data.get('id')
    else:
        customerid = '0'
    if customerid == '0':
        response = []
        response.append("No Customer Found")
        return Response(response, status=status.HTTP_201_CREATED)
    else:
        customer = Customer.objects.filter(id=customerid)
        serializer = CustomerSerializer(customer, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['GET', 'POST'])
def list(request):
    finaldata = []
    customers = Customer.objects.all().order_by('name')
    serializer = CustomerSerializer(customers, many=True)
    customerlist = serializer.data

    for x in customerlist:
        customerdata = x
        customerid = x['id']
        lastVisitQuery = Invoice.objects.filter(
            customerid=customerid).order_by('-invoice_date')[:1]
        invoiceSerializer = InvoiceSerializer(lastVisitQuery, many=True)
        lastvisitdata = invoiceSerializer.data

        lastYearSum = Invoice.objects.filter(customerid=customerid,
                                             invoice_date__range=["2020-01-01", "2021-01-31"]).aggregate(Sum('total_invoice_value'))

        overAllSum = Invoice.objects.filter(
            customerid=customerid).aggregate(Sum('total_invoice_value'))

        invoicedata = {
            'customerdata': customerdata,
            'lastvisit': lastvisitdata,
            'lastyear': lastYearSum,
            'overall': overAllSum
        }
        finaldata.append(invoicedata)

    return Response(finaldata)


@api_view(['GET', 'POST'])
def suggestions(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    customer = Customer.objects.filter(retailerid=loggedRetailer)
    serializer = CustomerSerializer(customer, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)
