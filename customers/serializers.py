from rest_framework import serializers
from .models import Customer


class CustomerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Customer
        fields = ('id', 'name', 'mobile', 'email', 'patientType',
                  'loyaltyDiscount', 'addressTitle', 'address', 'creditbalance','retailerid')


class CustomerListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Customer
        fields = ('id', 'name', 'mobile', 'email', 'patientType',
                  'loyaltyDiscount', 'addressTitle', 'address', 'creditbalance','retailerid')
