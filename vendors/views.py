from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Vendor
from .serializers import VendorSerializer
from rest_framework import status
from retailers.views import checkAuth


@api_view(['GET', 'POST'])
def list(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    vendors = Vendor.objects.filter(retailerid=loggedRetailer).order_by('name')
    serializer = VendorSerializer(vendors, many=True)
    return Response(serializer.data)


@api_view(['GET', 'POST'])
def detailsviamobile(request):
    vendors = Vendor.objects.filter(mobile=request.data.get('mobile'))
    serializer = VendorSerializer(vendors, many=True)
    return Response(serializer.data)


@api_view(['GET', 'POST'])
def save(request):
    data = request.data.getlist("data[]")
    vendorData = {
        'name': data[0],
        'mobile': data[1],
        'email': data[2],
        'address': data[3]
    }
    serializer = VendorSerializer(data=vendorData)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors)
