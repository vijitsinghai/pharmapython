# Generated by Django 3.1.4 on 2021-01-07 06:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0007_substitute'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='substitute',
            name='productid',
        ),
    ]
