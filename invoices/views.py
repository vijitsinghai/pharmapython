import json
from django.http import JsonResponse
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Invoice, InvoiceDetail, SalesOrder, SalesOrderDetails
from products.models import Products, Batch
from customers.models import Customer
from .serializers import InvoiceSerializer, InvoiceDetailSerializer, SalesOrderSerializer, SalesOrderDetailSerializer
from products.serializers import ProductsFilterSerializer, BatchSerializer, ProductsSerializer
from customers.serializers import CustomerSerializer
from retailers.views import checkAuth
from products.views import productStock, productStockBatch
from accounts.views import saveVoucher, addCustomerLedger, fixLedgerBalance, getSetup
from accounts.models import Ledger, Settings, VoucherDetail
from accounts.serializers import VoucherDetailSerializer

from rest_framework import status

# Create your views here.


@ api_view(['POST', 'GET'])
def invcount(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    invoiceprefix = getSetup(loggedRetailer, 'invoiceprefix')
    invoicenumbercount = getSetup(loggedRetailer,'invoicenumber')
    invoicenumber = invoiceprefix+"-"+invoicenumbercount
    return HttpResponse(invoicenumber)
    

@api_view(['POST'])
def save(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    thisCustomer = request.data.get('customerid')
    customerid = thisCustomer
    customerData = {
        'mobile': request.data.get(
            'mobile'),
        'name': request.data.get(
            'name'),
        'address': request.data.get(
            'customer_address'),
        'retailerid': loggedRetailer
    }
    if thisCustomer == "0":
        customerSerializer = CustomerSerializer(data=customerData)
        if customerSerializer.is_valid():
            customerinstance = customerSerializer.save()
            customerid = customerinstance.id
            if request.data.get('name') == '':
                thisName = request.data.get('mobile')
            else:
                thisName = request.data.get('name')
            addCustomerLedger(thisName, customerid, loggedRetailer)
            # return Response(customerSerializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(customerSerializer.errors, status=status.HTTP_400_BAD_REQUEST)
    else:
        customers = Customer.objects.get(id=request.data.get('customerid'))
        customerSerializer = CustomerSerializer(
            customers, data=customerData)
        if customerSerializer.is_valid():
            customerSerializer.save()
            # return Response(customerSerializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(customerSerializer.errors, status=status.HTTP_400_BAD_REQUEST)
    invoiceprefix = getSetup(loggedRetailer,'invoiceprefix')
    invoicenumbercount = getSetup(loggedRetailer,'invoicenumber')
    invoicenumber = invoiceprefix+"-"+invoicenumbercount
    invoicenumbercount = int(invoicenumbercount)
    invoicenumbercount = invoicenumbercount+1
    numbInstance = Settings.objects.filter(retailerid=loggedRetailer).get(subject='invoicenumber')
    numbInstance.value = invoicenumbercount
    numbInstance.save()
    invoiceData = {
        "customerid": customerid,
        "subtotal": request.data.get(
            "subtotal"),
        "discount": request.data.get(
            "discount"),
        "delivery_charge": request.data.get(
            "delivery_charge"),
        "payment_due": request.data.get(
            "payment_due"),
        "previous_credit": request.data.get(
            "previous_credit"),
        "outstanding": request.data.get(
            "outstanding"),
        "total_invoice_value": request.data.get(
            "total_invoice_value"),
        "invoice_date": request.data.get(
            "invoice_date"),
        "prescription_days": request.data.get(
            "prescription_days"),
        "payment_made": request.data.get(
            "payment_made"),
         "payment_made_cash": request.data.get(
            "payment_made_cash"),
        "payment_made_bank": request.data.get(
            "payment_made_bank"),
        'retailerid': loggedRetailer,
        'invoice_number': invoicenumber
    }
    if request.data.get('dbid') == "0":
        invoice_Serializer = InvoiceSerializer(data=invoiceData)
    else:
        dbid = request.data.get('dbid')
        dbid = int(dbid)
        thisInvoice = Invoice.objects.get(id=dbid)
        invoice_Serializer = InvoiceSerializer(
            thisInvoice, data=invoiceData, partial=True)
        InvoiceDetail.objects.filter(invoiceid=dbid).delete()

    if invoice_Serializer.is_valid():
        invoiceInstance = invoice_Serializer.save()
        invoice_id = invoiceInstance.id
        ledgertypes = []
        ledgerids = []
        debitamounts = []
        creditamounts = []
        notes = []
        entrytypes = []
        entryids = []
        ledgertypeids = []
        thisLedgerObject = Ledger.objects.filter(
            ledgertype='Customer').get(ledgertypeid=customerid)

        payment_made = float(request.data.get('payment_made'))
        payment_made_cash= float(request.data.get('payment_made_cash'))
        payment_made_bank = float(request.data.get('payment_made_bank'))
        invoice_value = float(request.data.get('total_invoice_value'))
        total_invoice_value = invoice_value
        delivery_charge = float(request.data.get('delivery_charge'))
        if payment_made == 0:
            debitamounts.append(request.data.get(
                "total_invoice_value"))
            creditamounts.append(0)
            ledgertypes.append('Customer')
            ledgertypeids.append(customerid)
            notes.append('New Due days Bill No: INV-'+str(invoice_id))
            entrytypes.append('Invoice')
            entryids.append(invoice_id)
            ledgerids.append(thisLedgerObject.id)
            vtype = "Sales"
            vsubtype = "Sales On Credit"
            vtitle = "Sales Voucher"

        elif payment_made != invoice_value:
            vtype = "Sales"
            vsubtype = "Sales On Partial Payment"
            vtitle = "Sales Voucher"

            diff = invoice_value - payment_made
            debitamounts.append(diff)
            creditamounts.append(0)
            ledgertypes.append('Customer')
            ledgertypeids.append(customerid)
            notes.append('New Due days Bill No: INV-'+str(invoice_id))
            entrytypes.append('Invoice')
            entryids.append(invoice_id)
            ledgerids.append(thisLedgerObject.id)

            if payment_made_cash != 0:
                debitamounts.append(payment_made_cash)
                creditamounts.append(0)
                ledgertypes.append('Cash')
                ledgertypeids.append('')
                notes.append('')
                entrytypes.append('Invoice')
                entryids.append(invoice_id)
                ledgerids.append(8)

            if payment_made_bank != 0:
                debitamounts.append(payment_made_bank)
                creditamounts.append(0)
                ledgertypes.append('Cash')
                ledgertypeids.append('')
                notes.append('')
                entrytypes.append('Invoice')
                entryids.append(invoice_id)
                ledgerids.append(28)



        elif payment_made == invoice_value:
            vtype = "Sales"
            vsubtype = "Sales On Full Payment"
            vtitle = "Sales Voucher"
            debitamounts.append(0)
            creditamounts.append(0)
            ledgertypes.append('Customer')
            ledgertypeids.append(customerid)
            notes.append('New Due days Bill No: INV-'+str(invoice_id))
            entrytypes.append('Invoice')
            entryids.append(invoice_id)
            ledgerids.append(thisLedgerObject.id)

            if payment_made_cash != 0:
                debitamounts.append(payment_made_cash)
                creditamounts.append(0)
                ledgertypes.append('Cash')
                ledgertypeids.append('')
                notes.append('')
                entrytypes.append('Invoice')
                entryids.append(invoice_id)
                ledgerids.append(8)

            if payment_made_bank != 0:
                debitamounts.append(payment_made_bank)
                creditamounts.append(0)
                ledgertypes.append('Cash')
                ledgertypeids.append('')
                notes.append('')
                entrytypes.append('Invoice')
                entryids.append(invoice_id)
                ledgerids.append(28)

        if delivery_charge != 0:
            debitamounts.append(0)
            creditamounts.append(delivery_charge)
            ledgertypes.append('Freight')
            ledgertypeids.append('')
            notes.append('')
            entrytypes.append('Invoice')
            entryids.append(invoice_id)
            ledgerids.append(12)


        only_sale_amount = total_invoice_value - delivery_charge
        taxvalue = only_sale_amount - only_sale_amount/1.05
        taxvalue = taxvalue/2
        taxvalue = round(taxvalue)

        gst_total = only_sale_amount - taxvalue - taxvalue

        debitamounts.append(0)
        creditamounts.append(gst_total)
        ledgertypes.append('GST LOCAL SALES')
        ledgertypeids.append('')
        notes.append('')
        entrytypes.append('Invoice')
        entryids.append(invoice_id)
        ledgerids.append(13)


        debitamounts.append(0)
        creditamounts.append(taxvalue)
        ledgertypes.append('CGST SALES TAX')
        ledgertypeids.append('')
        notes.append('')
        entrytypes.append('Invoice')
        entryids.append(invoice_id)
        ledgerids.append(14)

        debitamounts.append(0)
        creditamounts.append(taxvalue)
        ledgertypes.append('SGST SALES TAX')
        ledgertypeids.append('')
        notes.append('')
        entrytypes.append('Invoice')
        entryids.append(invoice_id)
        ledgerids.append(15)

        thisVoucherData = saveVoucher(vtype, vsubtype, vtitle, ledgertypes,
                    ledgerids, debitamounts, creditamounts, loggedRetailer, notes, entrytypes, entryids, ledgertypeids)

        thisInvoice = Invoice.objects.get(id=invoice_id)
        thisInvoice.voucherid = thisVoucherData[0]
        thisInvoice.vouchernumber = thisVoucherData[1]
        thisInvoice.save()

        fixLedgerBalance(thisLedgerObject.id,'Customer',customerid)
        # return Response(invoice_Serializer.data, status=status.HTTP_201_CREATED)
    else:
        return Response(invoice_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    medicineData = request.data.getlist('medicines')
    for x in medicineData:
        x = json.loads(x)
        for y in x:
            if y['deleted'] == "0":
                if y['newbatch'] == "1":
                    batch_data = {
                        'productid': y['productid'],
                        'number': y['batch'],
                        'expiry': y['expiry'],
                        'mrp': y['mrp'],
                        'discount': y['discount'],
                        'currentstock': 0,
                        'openingstock': 0,
                        'retailerid': loggedRetailer
                    }
                    batchSerializer = BatchSerializer(data=batch_data)
                    if batchSerializer.is_valid():
                        batchInstance = batchSerializer.save()
                        batchId = batchInstance.id
                        y['batch'] = batchId

                productConversionQuery = Products.objects.get(
                    id=y['productid'])
                conversion_ratio = productConversionQuery.conversion_ratio
                stockvalue = (
                    float(y['unit1']) * float(conversion_ratio)) + float(y['unit2'])
                thisData = {'invoiceid': invoice_id, 'productid': y['productid'],
                            'batchid': y['batch'], 'mrp': y['mrp'], 'primary_unit': y['unit1'], 'secondary_unit': y['unit2'], 'discount': y['discount'], 'subtotal': y['subtotal'],
                            'retailerid': loggedRetailer, 'stockvalue': stockvalue}
                invoicedetail_Serializer = InvoiceDetailSerializer(
                    data=thisData)
                if invoicedetail_Serializer.is_valid():
                    invoiceDetailInstance = invoicedetail_Serializer.save()
                    invoice_detail_id = invoiceDetailInstance.id

                    productStockBatch(
                        y['productid'], loggedRetailer, y['batch'])
                    productStock(y['productid'], loggedRetailer)

                else:
                    return Response(invoicedetail_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    saleOrderData = request.data.getlist('salesorder')

    if(len(saleOrderData) > 0):
        prepayment = request.data.get(
            "prepayment")
        saleData = {
            'prepayment': prepayment,
            'customerid': customerid,
            'retailerid': loggedRetailer
        }
        saleSave = SalesOrderSerializer(data=saleData)
        if saleSave.is_valid():
            saleInstance = saleSave.save()
            if prepayment != 0:
                thisCustomer = Customer.objects.get(id=customerid)
                previousCredit = thisCustomer.creditbalance
                if previousCredit == '':
                    previousCredit = 0
                newcredit = float(previousCredit) + float(prepayment)
                thisCustomerData = {
                    'creditbalance': newcredit
                }
                customerInstance = CustomerSerializer(
                    thisCustomer, data=thisCustomerData, partial=True)
                if customerInstance.is_valid():
                    customerInstance.save()
                else:
                    return Response(customerInstance.errors, status=status.HTTP_400_BAD_REQUEST)
            salesorderid = saleInstance.id
            for x in saleOrderData:
                x = json.loads(x)
                for y in x:
                    productConversionQuery = Products.objects.get(
                        id=y['productid'])
                    conversion_ratio = productConversionQuery.conversion_ratio
                    qtyTotal = (float(y['unit1']) * float(conversion_ratio)) + \
                        float(y['unit2'])

                    thisData = {'customerid': customerid, 'salesorderid': salesorderid, 'productid': y['productid'],
                                'mrp': y['mrp'], 'primary_unit': y['unit1'], 'secondary_unit': y['unit2'], 'total_units': qtyTotal, 'total_units_delivered': '0',
                                'retailerid': loggedRetailer}
                    salesOrder_Serializer = SalesOrderDetailSerializer(
                        data=thisData)
                    if salesOrder_Serializer.is_valid():
                        soInstance = salesOrder_Serializer.save()
                        salesOrderId = soInstance.id
                        # return Response(invoicedetail_Serializer.data, status=status.HTTP_201_CREATED)
                    else:
                        return Response(salesOrder_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(saleSave.errors, status=status.HTTP_400_BAD_REQUEST)

    saleOrderData = request.data.getlist('salesorder_old')
    if(len(saleOrderData) > 0):
        for x in saleOrderData:
            x = json.loads(x)
            for y in x:
                thisSalesOrderDetailsId = y['salesorderdetailsid']
                thisSalesOrderDetailsId = int(thisSalesOrderDetailsId)
                thisDelivery = y['thisdelivery']
                thisDelivery = float(thisDelivery)
                thisSalesInstance = SalesOrderDetails.objects.get(
                    id=thisSalesOrderDetailsId)
                thisTotalUnits = thisSalesInstance.total_units
                thisNewDelivery = (thisTotalUnits - thisDelivery)
                thisSalesData = {
                    'total_units_delivered': thisNewDelivery,
                    'associated_invoice': invoice_id
                }
                thisSalesInstance = SalesOrderDetailSerializer(
                    thisSalesInstance, data=thisSalesData, partial=True)
                if thisSalesInstance.is_valid():
                    thisSalesInstance.save()
                else:
                    return Response(thisSalesInstance.errors, status=status.HTTP_400_BAD_REQUEST)
    return HttpResponse('Successfully Updated')


@ api_view(['POST', 'GET'])
def addPayment(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    amount = float(request.data.get('amount'))
    invoiceid = request.data.get('invoiceid')
    invoicedata = Invoice.objects.get(id=invoiceid)
    customerid = invoicedata.customerid_id
    outstanding = invoicedata.outstanding
    payment_made = invoicedata.payment_made
    payment_made = float(payment_made)+amount
    outstanding = float(outstanding)-amount
    invoicedata.outstanding = outstanding
    invoicedata.payment_made = payment_made
    invoicedata.save()
    ledgertypes = []
    ledgerids = []
    debitamounts = []
    creditamounts = []
    notes = []
    entrytypes = []
    entryids = []
    ledgertypeids = []
    thisLedgerObject = Ledger.objects.filter(
        ledgertype='Customer').get(ledgertypeid=customerid)
    vtype = "Sales"
    vsubtype = "Payment"
    vtitle = "Payment Voucher"
    debitamounts.append(0)
    creditamounts.append(amount)
    ledgertypes.append('Customer')
    ledgertypeids.append(customerid)
    notes.append('Payment against: INV-'+str(invoiceid))
    entrytypes.append('Invoice')
    entryids.append(invoiceid)
    ledgerids.append(thisLedgerObject.id)

    debitamounts.append(amount)
    creditamounts.append(0)
    ledgertypes.append('Cash')
    ledgertypeids.append('')
    notes.append('')
    entrytypes.append('Invoice')
    entryids.append(invoiceid)
    ledgerids.append(8)


    t = saveVoucher(vtype, vsubtype, vtitle, ledgertypes,
                    ledgerids, debitamounts, creditamounts, loggedRetailer, notes, entrytypes, entryids, ledgertypeids)
    fixLedgerBalance(thisLedgerObject.id, 'Customer', customerid)
    return HttpResponse(t)


@ api_view(['POST', 'GET'])
def getlist(request):
    finaldata = []
    invoices = Invoice.objects.all().order_by('-invoice_date')
    serializer = InvoiceSerializer(invoices, many=True)

    t = serializer.data
    for x in t:
        thisdata = []
        thisdata.append(x)
        customerid = x['customerid']
        customers = Customer.objects.filter(id=customerid)
        customerserializer = CustomerSerializer(customers, many=True)
        thisdata.append(customerserializer.data)
        finaldata.append(thisdata)
    return Response(finaldata)


@ api_view(['POST', 'GET'])
def salesorderlist(request):
    finaldata = []
    invoices = SalesOrder.objects.all().order_by('-createdate')
    serializer = SalesOrderSerializer(invoices, many=True)

    t = serializer.data
    for x in t:
        thisdata = []
        thisdata.append(x)
        customerid = x['customerid']
        customers = Customer.objects.filter(id=customerid)
        customerserializer = CustomerSerializer(customers, many=True)
        thisdata.append(customerserializer.data)
        finaldata.append(thisdata)
    return Response(finaldata)


@ api_view(['POST', 'GET'])
def quickview(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    finaldata = []
    invoicedata = []
    invoice_id = request.data.get('id')
    invoices = InvoiceDetail.objects.filter(invoiceid=invoice_id)
    serializer = InvoiceDetailSerializer(invoices, many=True)
    idData = serializer.data


    vouchers = VoucherDetail.objects.filter(entrytype='Invoice').filter(entryid=invoice_id).filter(retailerid=loggedRetailer)
    voucherSer = VoucherDetailSerializer(vouchers, many=True)

    for x in idData:
        products = Products.objects.get(id=x['productid'])
        productData = ProductsSerializer(products)
        x['productdata'] = productData.data
        invoicedata.append(x)
    finaldata.append(invoicedata)
    finaldata.append(voucherSer.data)
    return Response(finaldata)


@ api_view(['POST', 'GET'])
def squickview(request):
    finaldata = []
    invoice_id = request.data.get('id')
    invoices = SalesOrderDetails.objects.filter(salesorderid=invoice_id)
    serializer = SalesOrderDetailSerializer(invoices, many=True)
    # return Response(serializer.data)
    idData = serializer.data

    for x in idData:
        products = Products.objects.get(id=x['productid'])
        productData = ProductsSerializer(products)
        x['productdata'] = productData.data
        finaldata.append(x)
    return Response(finaldata)


@ api_view(['POST', 'GET'])
def invoiceDetails(request):
    finaldata = []
    invoice_id = request.data.get('id')
    invoice = Invoice.objects.get(id=invoice_id)
    customerid = invoice.customerid
    serializer = InvoiceSerializer(invoice)
    finaldata.append(serializer.data)

    customerdata = Customer.objects.get(id=customerid)
    serializer = CustomerSerializer(customerdata)
    finaldata.append(serializer.data)

    invoices = InvoiceDetail.objects.filter(invoiceid=invoice_id)
    serializer = InvoiceDetailSerializer(invoices, many=True)
    idData = serializer.data

    for x in idData:
        products = Products.objects.get(id=x['productid'])
        productData = ProductsSerializer(products)
        x['productdata'] = productData.data
        finaldata.append(x)
    return Response(finaldata)


@api_view(['POST'])
def saveReturn(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    thisCustomer = request.data.get('customerid')
    customerid = thisCustomer
    customerData = {
        'mobile': request.data.get(
            'mobile'),
        'name': request.data.get(
            'name'),
        'address': request.data.get(
            'customer_address'),
        'retailerid': loggedRetailer
    }
    if thisCustomer == "0":
        checkCustomer = Customer.objects.get(mobile=request.data.get('mobile'))
        if checkCustomer:
            customerid = checkCustomer.id
        else:
            customerSerializer = CustomerSerializer(data=customerData)
            if customerSerializer.is_valid():
                customerinstance = customerSerializer.save()
                customerid = customerinstance.id
                if request.data.get('name') == '':
                    thisName = request.data.get('mobile')
                else:
                    thisName = request.data.get('name')
                addCustomerLedger(thisName, customerid, loggedRetailer)
            # return Response(customerSerializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(customerSerializer.errors, status=status.HTTP_400_BAD_REQUEST)
    else:
        customers = Customer.objects.get(id=request.data.get('customerid'))
        customerSerializer = CustomerSerializer(
            customers, data=customerData)
        if customerSerializer.is_valid():
            customerSerializer.save()
            # return Response(customerSerializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(customerSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

    invoiceData = {
        "customerid": customerid,
        "subtotal": request.data.get(
            "subtotal"),
        "discount": request.data.get(
            "discount"),
        "delivery_charge": request.data.get(
            "delivery_charge"),
        "payment_due": request.data.get(
            "payment_due"),
        "previous_credit": request.data.get(
            "previous_credit"),
        "outstanding": request.data.get(
            "outstanding"),
        "total_invoice_value": request.data.get(
            "total_invoice_value"),
        "invoice_date": request.data.get(
            "invoice_date"),
        "prescription_days": request.data.get(
            "prescription_days"),
        "payment_made": request.data.get(
            "payment_made"),
        'retailerid': loggedRetailer
    }
    if request.data.get('dbid') == "0":
        invoice_Serializer = InvoiceSerializer(data=invoiceData)
    else:
        dbid = request.data.get('dbid')
        dbid = int(dbid)
        thisInvoice = Invoice.objects.get(id=dbid)
        invoice_Serializer = InvoiceSerializer(
            thisInvoice, data=invoiceData, partial=True)
        InvoiceDetail.objects.filter(invoiceid=dbid).delete()

    if invoice_Serializer.is_valid():
        invoiceInstance = invoice_Serializer.save()
        invoice_id = invoiceInstance.id
        ledgertypes = []
        ledgerids = []
        debitamounts = []
        creditamounts = []
        notes = []
        entrytypes = []
        entryids = []
        ledgertypeids = []
        thisLedgerObject = Ledger.objects.filter(
            ledgertype='Customer').get(ledgertypeid=customerid)

        payment_made = float(request.data.get('payment_made'))
        payment_made = payment_made * -1
        invoice_value = float(request.data.get('total_invoice_value'))
        invoice_value = invoice_value * -1
        total_invoice_value = invoice_value
        delivery_charge = float(request.data.get('delivery_charge'))
        if payment_made == 0:
            debitamounts.append(0)
            creditamounts.append(total_invoice_value)
            ledgertypes.append('Customer')
            ledgertypeids.append(customerid)
            notes.append('Sales Return : INV-'+str(invoice_id))
            entrytypes.append('Invoice')
            entryids.append(invoice_id)
            ledgerids.append(thisLedgerObject.id)
            vtype = "Sales Return"
            vsubtype = "Sales Return Credit Note"
            vtitle = "Customer Credit Note"

        elif payment_made != invoice_value:
            vtype = "Sales Return"
            vsubtype = "Sales Return Credit Note (Partial)"
            vtitle = "Customer Credit Note"

            diff = invoice_value - payment_made
            debitamounts.append(0)
            creditamounts.append(diff)
            ledgertypes.append('Customer')
            ledgertypeids.append(customerid)
            notes.append('Sales Return : INV-'+str(invoice_id))
            entrytypes.append('Invoice')
            entryids.append(invoice_id)
            ledgerids.append(thisLedgerObject.id)

            debitamounts.append(0)
            creditamounts.append(payment_made)
            ledgertypes.append('Cash')
            ledgertypeids.append('')
            notes.append('')
            entrytypes.append('Invoice')
            entryids.append(invoice_id)
            ledgerids.append(8)

        elif payment_made == invoice_value:
            vtype = "Sales Return"
            vsubtype = "Sales Return Full Payment Done"
            vtitle = "Sales Return Voucher"
            debitamounts.append(0)
            creditamounts.append(0)
            ledgertypes.append('Customer')
            ledgertypeids.append(customerid)
            notes.append('Sales Return : INV-'+str(invoice_id))
            entrytypes.append('Invoice')
            entryids.append(invoice_id)
            ledgerids.append(thisLedgerObject.id)

            debitamounts.append(0)
            creditamounts.append(payment_made)
            ledgertypes.append('Cash')
            ledgertypeids.append('')
            notes.append('')
            entrytypes.append('Invoice')
            entryids.append(invoice_id)
            ledgerids.append(8)

        debitamounts.append(total_invoice_value)
        creditamounts.append(0)
        ledgertypes.append('GST LOCAL SALES')
        ledgertypeids.append('')
        notes.append('')
        entrytypes.append('Invoice')
        entryids.append(invoice_id)
        ledgerids.append(13)

        taxvalue = total_invoice_value - total_invoice_value/1.05
        taxvalue = taxvalue/2
        taxvalue = round(taxvalue)

        debitamounts.append(taxvalue)
        creditamounts.append(0)
        ledgertypes.append('CGST SALES TAX')
        ledgertypeids.append('')
        notes.append('')
        entrytypes.append('Invoice')
        entryids.append(invoice_id)
        ledgerids.append(14)

        debitamounts.append(taxvalue)
        creditamounts.append(0)
        ledgertypes.append('SGST SALES TAX')
        ledgertypeids.append('')
        notes.append('')
        entrytypes.append('Invoice')
        entryids.append(invoice_id)
        ledgerids.append(15)

        saveVoucher(vtype, vsubtype, vtitle, ledgertypes,
                    ledgerids, debitamounts, creditamounts, loggedRetailer, notes, entrytypes, entryids, ledgertypeids)
        fixLedgerBalance(thisLedgerObject.id, 'Customer', customerid)
        # return Response(invoice_Serializer.data, status=status.HTTP_201_CREATED)
    else:
        return Response(invoice_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    medicineData = request.data.getlist('medicines')
    for x in medicineData:
        x = json.loads(x)
        for y in x:
            if y['deleted'] == "0":
                if y['newbatch'] == "1":
                    batch_data = {
                        'productid': y['productid'],
                        'number': y['batch'],
                        'expiry': y['expiry'],
                        'mrp': y['mrp'],
                        'discount': y['discount'],
                        'currentstock': 0,
                        'openingstock': 0,
                        'retailerid': loggedRetailer
                    }
                    batchSerializer = BatchSerializer(data=batch_data)
                    if batchSerializer.is_valid():
                        batchInstance = batchSerializer.save()
                        batchId = batchInstance.id
                        y['batch'] = batchId

                productConversionQuery = Products.objects.get(
                    id=y['productid'])
                conversion_ratio = productConversionQuery.conversion_ratio
                stockvalue = (
                    float(y['unit1']) * float(conversion_ratio)) + float(y['unit2'])
                thisData = {'invoiceid': invoice_id, 'productid': y['productid'],
                            'batchid': y['batch'], 'mrp': y['mrp'], 'primary_unit': y['unit1'], 'secondary_unit': y['unit2'], 'discount': y['discount'], 'subtotal': y['subtotal'],
                            'retailerid': loggedRetailer, 'stockvalue': stockvalue}
                invoicedetail_Serializer = InvoiceDetailSerializer(
                    data=thisData)
                if invoicedetail_Serializer.is_valid():
                    invoiceDetailInstance = invoicedetail_Serializer.save()
                    invoice_detail_id = invoiceDetailInstance.id

                    productStockBatch(
                        y['productid'], loggedRetailer, y['batch'])
                    productStock(y['productid'], loggedRetailer)

                else:
                    return Response(invoicedetail_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    saleOrderData = request.data.getlist('salesorder')

    if(len(saleOrderData) > 0):
        prepayment = request.data.get(
            "prepayment")
        saleData = {
            'prepayment': prepayment,
            'customerid': customerid,
            'retailerid': loggedRetailer
        }
        saleSave = SalesOrderSerializer(data=saleData)
        if saleSave.is_valid():
            saleInstance = saleSave.save()
            if prepayment != 0:
                thisCustomer = Customer.objects.get(id=customerid)
                previousCredit = thisCustomer.creditbalance
                if previousCredit == '':
                    previousCredit = 0
                newcredit = float(previousCredit) + float(prepayment)
                thisCustomerData = {
                    'creditbalance': newcredit
                }
                customerInstance = CustomerSerializer(
                    thisCustomer, data=thisCustomerData, partial=True)
                if customerInstance.is_valid():
                    customerInstance.save()
                else:
                    return Response(customerInstance.errors, status=status.HTTP_400_BAD_REQUEST)
            salesorderid = saleInstance.id
            for x in saleOrderData:
                x = json.loads(x)
                for y in x:
                    productConversionQuery = Products.objects.get(
                        id=y['productid'])
                    conversion_ratio = productConversionQuery.conversion_ratio
                    qtyTotal = (float(y['unit1']) * float(conversion_ratio)) + \
                        float(y['unit2'])

                    thisData = {'customerid': customerid, 'salesorderid': salesorderid, 'productid': y['productid'],
                                'mrp': y['mrp'], 'primary_unit': y['unit1'], 'secondary_unit': y['unit2'], 'total_units': qtyTotal, 'total_units_delivered': '0',
                                'retailerid': loggedRetailer}
                    salesOrder_Serializer = SalesOrderDetailSerializer(
                        data=thisData)
                    if salesOrder_Serializer.is_valid():
                        soInstance = salesOrder_Serializer.save()
                        salesOrderId = soInstance.id
                        # return Response(invoicedetail_Serializer.data, status=status.HTTP_201_CREATED)
                    else:
                        return Response(salesOrder_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(saleSave.errors, status=status.HTTP_400_BAD_REQUEST)

    saleOrderData = request.data.getlist('salesorder_old')
    if(len(saleOrderData) > 0):
        for x in saleOrderData:
            x = json.loads(x)
            for y in x:
                thisSalesOrderDetailsId = y['salesorderdetailsid']
                thisSalesOrderDetailsId = int(thisSalesOrderDetailsId)
                thisDelivery = y['thisdelivery']
                thisDelivery = float(thisDelivery)
                thisSalesInstance = SalesOrderDetails.objects.get(
                    id=thisSalesOrderDetailsId)
                thisTotalUnits = thisSalesInstance.total_units
                thisNewDelivery = (thisTotalUnits - thisDelivery)
                thisSalesData = {
                    'total_units_delivered': thisNewDelivery,
                    'associated_invoice': invoice_id
                }
                thisSalesInstance = SalesOrderDetailSerializer(
                    thisSalesInstance, data=thisSalesData, partial=True)
                if thisSalesInstance.is_valid():
                    thisSalesInstance.save()
                else:
                    return Response(thisSalesInstance.errors, status=status.HTTP_400_BAD_REQUEST)
    return HttpResponse('Successfully Updated')
