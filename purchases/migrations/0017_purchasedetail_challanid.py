# Generated by Django 3.1.4 on 2021-03-03 16:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('purchases', '0016_remove_purchasedetail_challanid'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchasedetail',
            name='challanid',
            field=models.ForeignKey(default=94, on_delete=django.db.models.deletion.CASCADE, related_name='challan', to='purchases.purchase'),
            preserve_default=False,
        ),
    ]
