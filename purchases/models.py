from django.db import models
from vendors.models import Vendor
from products.models import Products, Batch
from retailers.models import Retailer

# Create your models here.


class Purchase(models.Model):
    vendorid = models.ForeignKey(
        Vendor, related_name='pruchase_vendor', on_delete=models.CASCADE, default="1")
    purchase_type = models.CharField(max_length=200, null=False,
                                     blank=True)
    invoice_number = models.CharField(max_length=200, null=False,
                                      blank=True)
    subtotal = models.CharField(max_length=200, null=False,
                                blank=True)
    discount = models.CharField(max_length=200, null=False,
                                blank=True)
    gst = models.CharField(max_length=200, null=False,
                           blank=True)
    total_invoice_value = models.CharField(max_length=200, null=False,
                                           blank=True)
    invoice_date = models.DateTimeField(null=False,
                                        blank=False, auto_now_add=True)
    payment_made = models.CharField(max_length=200, null=True,
                                    blank=True)
    converted = models.BooleanField()
    associated_purchase_id = models.IntegerField(null=True,blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer, on_delete=models.CASCADE)

    def __int__(self):
        return self.id



class PurchaseDetail(models.Model):
    purchaseid = models.ForeignKey(
        Purchase, related_name='purchase', on_delete=models.CASCADE)
    challanid = models.ForeignKey(
        Purchase, related_name='challan', on_delete=models.CASCADE)
    productid = models.ForeignKey(
        Products, related_name='puchase_product', on_delete=models.CASCADE)
    batchid = models.ForeignKey(
        Batch, related_name='puchase_batch', on_delete=models.CASCADE)
    mrp = models.CharField(max_length=200, null=False,
                           blank=True)
    quantity = models.CharField(max_length=200, null=False,
                                blank=True)
    quantity_free = models.CharField(max_length=200, null=False,
                                     blank=True)
    rate = models.CharField(max_length=200, null=False,
                            blank=True)

    discount = models.CharField(max_length=200, null=False,
                                blank=True)
    subtotal = models.CharField(max_length=200, null=False,
                                blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer, on_delete=models.CASCADE)
    stockvalue = models.CharField(max_length=200, null=False,
                                  blank=True)

    def __int__(self):
        return self.id


class PurchaseReturn(models.Model):

    vendorid = models.ForeignKey(
        Vendor, related_name='pruchase_return_vendor_id', on_delete=models.CASCADE, default="1")
    subtotal = models.CharField(max_length=200, null=False,
                                blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(Retailer, on_delete=models.CASCADE)
    creditvalue = models.CharField(max_length=200, null=False,
                                blank=True)
    used = models.CharField(max_length=200, null=False,
                                blank=True)
    creditnumber = models.CharField(max_length=200, null=False,
                            blank=True)

    def __int__(self):
        return self.id



class PurchaseReturnDetail(models.Model):

    purchasereturnid = models.ForeignKey(
        PurchaseReturn, on_delete=models.CASCADE, null=True)

    productid = models.ForeignKey(
        Products, related_name='puchase_return_product', on_delete=models.CASCADE)
    batchid = models.ForeignKey(
        Batch, related_name='puchase_return_batch', on_delete=models.CASCADE)
    mrp = models.CharField(max_length=200, null=False,
                           blank=True)
    quantity = models.CharField(max_length=200, null=False,
                                blank=True)
    vendorid = models.ForeignKey(
        Vendor, related_name='pruchase_return_vendor', on_delete=models.CASCADE, default="1")
    subtotal = models.CharField(max_length=200, null=False,
                                blank=True)
    billno = models.CharField(max_length=200, null=False,
                                blank=True)

    draft = models.BooleanField()
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer, on_delete=models.CASCADE)
    stockvalue = models.CharField(max_length=200, null=False,
                                  blank=True)

    def __int__(self):
        return self.id





