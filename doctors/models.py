from django.db import models
from retailers.models import Retailer

# Create your models here.


class Doctor(models.Model):
    name = models.CharField(max_length=200)
    mobile = models.CharField(max_length=200, null=False,
                              blank=True)
    email = models.CharField(max_length=200, null=False,
                             blank=True)
    doctor_type = models.CharField(max_length=200, null=False,
                                   blank=True)
    address = models.CharField(max_length=200, null=False,
                               blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer, related_name='doctor_retailer', on_delete=models.CASCADE)

    def __int__(self):
        return self.id
