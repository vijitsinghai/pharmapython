from django.contrib import admin

from .models import Purchase, PurchaseDetail, PurchaseReturn, PurchaseReturnDetail
# Register your models here.
admin.site.register(Purchase)
admin.site.register(PurchaseDetail)
admin.site.register(PurchaseReturn)
admin.site.register(PurchaseReturnDetail)
