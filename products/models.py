from django.db import models
from retailers.models import Retailer

# Create your models here.


class Salt(models.Model):

    name = models.CharField(max_length=200)
    theraputic = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class ProductCompany(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Unit(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Products(models.Model):
    name = models.CharField(max_length=200)
    packaging = models.CharField(max_length=200)

    unit = models.ForeignKey(
        Unit, related_name='unit', on_delete=models.RESTRICT)
    primary_unit = models.CharField(max_length=200, null=False,
                                    blank=True)
    secondary_unit = models.CharField(max_length=200, null=False,
                                      blank=True)
    conversion_ratio = models.CharField(max_length=200, null=False,
                                        blank=True)
    company = models.ForeignKey(
        ProductCompany, related_name='product_company', on_delete=models.RESTRICT)
    narcotic = models.BooleanField()
    rack = models.CharField(max_length=200, null=False,
                            blank=True)
    hsn = models.CharField(max_length=200, null=False,
                           blank=True)
    tax = models.CharField(max_length=200, null=False,
                           blank=True)
    opening_stock = models.CharField(max_length=200, null=False,
                                     blank=True)
    current_stock = models.CharField(max_length=200, null=False,
                                     blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)

    def __int__(self):
        return self.id

    def __str__(self):
        return self.name


class Substitute(models.Model):
    productid = models.ForeignKey(
        Products, related_name='substituteparent', on_delete=models.CASCADE)
    substituteid = models.ForeignKey(
        Products, related_name='substitute', on_delete=models.CASCADE)

    def __int__(self):
        return self.substituteid


class Batch(models.Model):
    productid = models.ForeignKey(
        Products, related_name='batch', on_delete=models.CASCADE)
    number = models.CharField(max_length=200, blank=True)
    expiry = models.DateField(null=False,
                               blank=True)
    mrp = models.CharField(max_length=200, blank=True)
    discount = models.CharField(max_length=200, blank=True)
    currentstock = models.CharField(max_length=200, null=True, blank=True)
    openingstock = models.CharField(max_length=200, null=True, blank=True)
    retailerid = models.ForeignKey(
        Retailer, on_delete=models.CASCADE)

    def __int__(self):
        return self.id

    def __str__(self):
        return self.number


class SaltProductMapping(models.Model):
    productid = models.ForeignKey(
        Products, related_name='salt_product', on_delete=models.CASCADE)
    saltid = models.ForeignKey(
        Salt, related_name='saltdata', on_delete=models.CASCADE)

    def __str__(self):
        return '%s : %s' % (self.productid, self.saltid)


class RetailerProductMapping(models.Model):
    retailerid = models.ForeignKey(
        Retailer,  on_delete=models.CASCADE)
    productid = models.ForeignKey(
        Products, on_delete=models.CASCADE)
    currentstock = models.CharField(max_length=200, null=True)
    openingstock = models.CharField(max_length=200, null=True)
    rack = models.CharField(max_length=200, null=True)
    alias = models.CharField(max_length=200, null=True)

    def __str__(self):
        return '%s : %s' % (self.productid, self.retailerid)
