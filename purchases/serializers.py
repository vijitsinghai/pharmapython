from rest_framework import serializers
from .models import Purchase, PurchaseDetail, PurchaseReturn, PurchaseReturnDetail
from products.models import Batch
from vendors.serializers import VendorSerializer
from products.serializers import ProductsSerializer, BatchSerializer


class PurchaseSerializer(serializers.ModelSerializer):

    vendorData = VendorSerializer(source="vendorid", read_only=True)
    class Meta:
        model = Purchase
        fields = ('id', 'vendorid', 'invoice_number', 'subtotal',
                  'discount', 'gst', 'total_invoice_value', 'invoice_date', 'payment_made', 'purchase_type', 'converted','vendorData','retailerid','associated_purchase_id')


class PurchaseDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = PurchaseDetail
        fields = ('id', 'purchaseid', 'productid', 'batchid', 'mrp',
                  'quantity', 'quantity_free', 'rate', 'discount', 'subtotal', 'retailerid','challanid')


class PurchaseReturnDetailSerializer(serializers.ModelSerializer):

    productData = ProductsSerializer(source="productid", read_only=True)
    batchData = BatchSerializer(source="batchid", read_only=True)
    vendorData = VendorSerializer(source="vendorid", read_only=True)
    class Meta:
        model = PurchaseReturnDetail
        fields = ('id',  'productid', 'batchid', 'mrp',
                  'quantity',  'vendorid', 'subtotal', 'draft', 'productData', 'batchData', 'billno', 'vendorData', 'retailerid','stockvalue')

class PurchaseReturnSerializer(serializers.ModelSerializer):

    vendorData = VendorSerializer(source="vendorid", read_only=True)
    class Meta:
        model = PurchaseReturn
        fields = ('id',  'vendorid', 'subtotal', 'retailerid', 'vendorData','createdate','creditnumber','creditvalue','used')
