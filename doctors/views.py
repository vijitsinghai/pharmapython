from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Doctor
from .serializers import DoctorSerializer
from rest_framework import status

# Create your views here.


@api_view(['GET', 'POST'])
def list(request):
    doctors = Doctor.objects.all()
    serializer = DoctorSerializer(doctors, many=True)
    return Response(serializer.data)


@api_view(['GET', 'POST'])
def save(request):
    data = request.data.getlist("data[]")
    doctorData = {
        'name': data[0],
        'mobile': data[1],
        'email': data[2],
        'doctor_type': data[3],
        'address': data[4]
    }
    serializer = DoctorSerializer(data=doctorData)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors)
