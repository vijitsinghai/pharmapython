# Generated by Django 3.1.4 on 2021-03-06 09:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('retailers', '0003_auto_20210302_1239'),
        ('accounts', '0005_ledger_accountgroupid'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='voucher',
            name='amount',
        ),
        migrations.RemoveField(
            model_name='voucher',
            name='amounttype',
        ),
        migrations.RemoveField(
            model_name='voucher',
            name='ledgerid',
        ),
        migrations.RemoveField(
            model_name='voucher',
            name='ledgertype',
        ),
        migrations.RemoveField(
            model_name='voucher',
            name='paymentyet',
        ),
        migrations.CreateModel(
            name='VoucherDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ledgertype', models.CharField(blank=True, max_length=200)),
                ('ledgerid', models.CharField(blank=True, max_length=200)),
                ('debit', models.CharField(blank=True, max_length=200)),
                ('credit', models.CharField(blank=True, max_length=200)),
                ('notes', models.CharField(blank=True, max_length=200)),
                ('createdate', models.DateTimeField(auto_now_add=True)),
                ('retailerid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='retailers.retailer')),
                ('voucherid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.voucher')),
            ],
        ),
    ]
