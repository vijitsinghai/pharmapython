from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'invoices'
urlpatterns = [
    path('save/', views.save,
         name='save'),
    path('list/', views.getlist,
         name='list'),
    path('quickview/', views.quickview,
         name='quickview'),
    path('salesorderlist/', views.salesorderlist,
         name='salesorderlist'),
    path('salesorder_quickview/', views.squickview,
         name='salesorder_quickview'),

    path('invoicedetails/', views.invoiceDetails,
         name='invoiceDetails'),
    path('add_payment/', views.addPayment,
         name='add_payment'),
    path('save_return/', views.saveReturn,
         name='save_return'),
    path('invcount/', views.invcount,
         name='invcount'),

         




]
