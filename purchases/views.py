import json
from django.http import JsonResponse
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Purchase, PurchaseDetail, PurchaseReturn, PurchaseReturnDetail
from .serializers import PurchaseSerializer, PurchaseDetailSerializer, PurchaseReturnDetailSerializer, PurchaseReturnSerializer
from vendors.serializers import VendorSerializer
from vendors.models import Vendor
from rest_framework import status
from products.models import Products
from products.serializers import BatchSerializer, ProductsSerializer, ProductsFilterSerializer
from retailers.views import checkAuth
from django.db.models import Sum
from products.views import productStock, productStockBatch
from accounts.views import saveVoucher, addVendorLedger, fixLedgerBalance
from accounts.models import Ledger




def saveSelectedChallans(challanid,loggedRetailer, purchaseid):
    challanData = Purchase.objects.get(id=challanid)
    thisdata = {
                    'converted': 1,
                    'retailerid' : loggedRetailer,
                    'associated_purchase_id' : purchaseid
                }
    challanSer = PurchaseSerializer(
    challanData, data=thisdata, partial=True)
    if challanSer.is_valid():
        challanSer.save()
        challanDetails = PurchaseDetail.objects.filter(
            challanid=challanid).update(purchaseid=purchaseid)
        return True
    else:
        return False

@api_view(['GET', 'POST'])
def save(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    if request.data:

        ##Save Vendor Data
        vendorid = request.data.get('vendorid')
        vendordata = {
            'mobile': request.data.get('mobile'),
            'name': request.data.get('name'),
            'email': request.data.get('vendor_email'),
            'mobile': request.data.get('mobile'),
            'retailerid' : loggedRetailer 
        }
        if(vendorid == '0'):
            vendorSer = VendorSerializer(data=vendordata)
            addVendorLedger(thisName, vendorid, loggedRetailer)
        else:
            vendor = Vendor.objects.get(id=vendorid)
            vendorSer = VendorSerializer(vendor, data=vendordata)
        if vendorSer.is_valid():
            vendorInstance = vendorSer.save()
            vendorid = vendorInstance.id
            if request.data.get('name') == '':
                thisName = request.data.get('mobile')
            else:
                thisName = request.data.get('name')
            
        else:
            return Response(vendorSer.errors, status=status.HTTP_400_BAD_REQUEST)

        ##Save Purchase Data
        if request.data.get('purchase_type') == 'Bill':
            converted = 1
        else:
            converted = 0
        payment_made = request.data.get('payment_made')
        if payment_made == "":
            payment_made = 0
        
        pruchasedata = {
            'vendorid': vendorid, 'subtotal': request.data.get('subtotal'),
            'discount': request.data.get('discount'), 'gst': request.data.get('gst'), 'total_invoice_value': request.data.get('total_invoice_value'), 'invoice_date': request.data.get('invoice_date'), 'purchase_type': request.data.get('purchase_type'), 'invoice_number': request.data.get('invoice_number'), 'converted': converted, 'retailerid': loggedRetailer,
            'payment_made' : payment_made}
        serializer = PurchaseSerializer(data=pruchasedata)
        if serializer.is_valid():
            purchaseInstance = serializer.save()
            purchaseid = purchaseInstance.id
            if request.data.get('purchase_type') == 'Bill':
                ledgertypes = []
                ledgerids = []
                debitamounts = []
                creditamounts = []
                notes = []
                entrytypes = []
                entryids = []
                ledgertypeids = []
                thisisnew = True
                thisLedgerObject = Ledger.objects.filter(ledgertype='Supplier').get(ledgertypeid=vendorid)

                payment_made = float(request.data.get('payment_made'))
                invoice_value = float(request.data.get('total_invoice_value'))
                total_invoice_value = invoice_value
                if payment_made == 0:
                    debitamounts.append(0)
                    creditamounts.append(total_invoice_value)
                    ledgertypes.append('Supplier')
                    ledgertypeids.append(vendorid)
                    notes.append('Purchase credit : PR-'+str(purchaseid))
                    entrytypes.append('Purchase')
                    entryids.append(purchaseid)
                    ledgerids.append(thisLedgerObject.id)
                    vtype = "Purchase"
                    vsubtype = "Sales On Credit"
                    vtitle = "Purchase Voucher"

                elif payment_made != invoice_value:
                    vtype = "Purchase"
                    vsubtype = "Sales On Partial Payment"
                    vtitle = "Purchase Voucher"

                    diff = invoice_value - payment_made
                    debitamounts.append(0)
                    creditamounts.append(diff)
                    ledgertypes.append('Supplier')
                    ledgertypeids.append(vendorid)
                    notes.append('Purchase : PR-'+str(purchaseid))
                    entrytypes.append('Purchase')
                    entryids.append(purchaseid)
                    ledgerids.append(thisLedgerObject.id)

                    debitamounts.append(0)
                    creditamounts.append(payment_made)
                    ledgertypes.append('Cash')
                    ledgertypeids.append('')
                    notes.append('')
                    entrytypes.append('Purchase')
                    entryids.append(purchaseid)
                    ledgerids.append(8)

                elif payment_made == invoice_value:
                    vtype = "Purchase"
                    vsubtype = "Sales On Partial Payment"
                    vtitle = "Purchase Voucher"
                    debitamounts.append(0)
                    creditamounts.append(0)
                    ledgertypes.append('Supplier')
                    ledgertypeids.append(vendorid)
                    notes.append('Purchase : PR-'+str(purchaseid))
                    entrytypes.append('Purchase')
                    entryids.append(purchaseid)
                    ledgerids.append(thisLedgerObject.id)


                    debitamounts.append(0)
                    creditamounts.append(payment_made)
                    ledgertypes.append('Cash')
                    ledgertypeids.append('')
                    notes.append('')
                    entrytypes.append('Purchase')
                    entryids.append(purchaseid)
                    ledgerids.append(8)

                creditamounts.append(0)
                debitamounts.append(total_invoice_value)
                ledgertypes.append('GST LOCAL SALES')
                ledgertypeids.append('')
                notes.append('')
                entrytypes.append('Purchase')
                entryids.append(purchaseid)
                ledgerids.append(13)

                taxvalue = total_invoice_value - total_invoice_value/1.05
                taxvalue = taxvalue/2
                taxvalue = round(taxvalue)

                creditamounts.append(0)
                debitamounts.append(taxvalue)
                ledgertypes.append('CGST SALES TAX')
                ledgertypeids.append('')
                notes.append('')
                entrytypes.append('Purchase')
                entryids.append(purchaseid)
                ledgerids.append(14)

                creditamounts.append(0)
                debitamounts.append(taxvalue)
                ledgertypes.append('SGST SALES TAX')
                ledgertypeids.append('')
                notes.append('')
                entrytypes.append('Purchase')
                entryids.append(purchaseid)
                ledgerids.append(15)

                saveVoucher(vtype, vsubtype, vtitle, ledgertypes,
                            ledgerids, debitamounts, creditamounts, loggedRetailer, notes, entrytypes, entryids, ledgertypeids)
                fixLedgerBalance(thisLedgerObject.id,'Supplier',vendorid)


        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        ##Save Selected Challans Data
        selectedchallans = request.data.get('selectedChallans')
        selectedchallans = selectedchallans.split(",")
        for s in selectedchallans:
            if s != '':
                s = int(s)
                saveSelectedChallans(s, loggedRetailer, purchaseid)

        ##Save Medicine Data
        medicineData = request.data.getlist('medicines')
        for x in medicineData:
            x = json.loads(x)
            for y in x:
                if(y['challandetailsid'] != "0"):
                    """
                    thischallanid = int(y['challandetailsid'])
                    thisBatch = PurchaseDetail.objects.get(id=thischallanid)
                    getbatch = PurchaseDetailSerializer(thisBatch)
                    batchId = getbatch.data['batchid']
                    thisData = {'purchaseid': purchaseid, 'productid': y['productid'],
                                'batchid': batchId, 'mrp': y['mrp'], 'quantity': y['qty'], 'quantity_free': y['qtyfree'], 'rate': y['rate'], 'discount': y['discount'], 'subtotal': y['subtotal'], 'retailerid': loggedRetailer}
                    """        
                else:
                    ##Save Batch Data
                    if y['batchdbid'] != "0":
                        batchId = int(y['batchdbid'])
                    else:                        
                        batch_data = {
                            'productid': y['productid'],
                            'number': y['number'],
                            'expiry': y['expiry'],
                            'mrp': y['mrp'],
                            'discount': y['discount'],
                            'currentstock': "0",
                            'openingstock': "0",
                            'retailerid': loggedRetailer
                        }
                        batchSerializer = BatchSerializer(data=batch_data)
                        if batchSerializer.is_valid():
                            batchInstance = batchSerializer.save()
                            batchId = batchInstance.id

                    ##Save Purchase Details Data
                    thisData = {'purchaseid': purchaseid, 'productid': y['productid'], 'batchid': batchId, 'mrp': y['mrp'], 'quantity': y['qty'], 'quantity_free': y[
                        'qtyfree'], 'rate': y['rate'], 'discount': y['discount'], 'subtotal': y['subtotal'], 'retailerid': loggedRetailer, 'challanid': purchaseid}
                    purchasedetail_Serializer = PurchaseDetailSerializer(data=thisData)
                    if purchasedetail_Serializer.is_valid():
                        purchaseDetailInstance = purchasedetail_Serializer.save()
                        addStockValue(y['productid'], y['qty'], y['qtyfree'], purchaseDetailInstance.id )
                        purchase_detail_id = purchaseDetailInstance.id
                        productStockBatch(y['productid'], loggedRetailer, batchId)
                        productStock(y['productid'], loggedRetailer)
                    else:
                        return Response(purchasedetail_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        finaldata = []
        finaldata.append('Successfully Saved')
        return HttpResponse(finaldata)
    else:
        finaldata = []
        finaldata.append('No Data Recived')
        return HttpResponse(finaldata)


@ api_view(['POST', 'GET'])
def getlist(request):
    finaldata = []
    purchases = Purchase.objects.all().order_by('-invoice_date')
    serializer = PurchaseSerializer(purchases, many=True)

    t = serializer.data
    for x in t:
        thisdata = []
        thisdata.append(x)
        vendorid = x['vendorid']
        vendors = Vendor.objects.filter(id=vendorid)
        vendorserializer = VendorSerializer(vendors, many=True)
        thisdata.append(vendorserializer.data)
        finaldata.append(thisdata)
    return Response(finaldata)


@ api_view(['POST', 'GET'])
def quickview(request):
    finaldata = []
    purchase_id = request.data.get('id')
    thisPurchase = Purchase.objects.get(id=purchase_id)
    thisPurchaseType = thisPurchase.purchase_type
    if thisPurchaseType == 'Bill':        
        purchases = PurchaseDetail.objects.filter(purchaseid=purchase_id)
    else:
        purchases = PurchaseDetail.objects.filter(challanid=purchase_id)
    serializer = PurchaseDetailSerializer(purchases, many=True)
    # return Response(serializer.data)
    idData = serializer.data
    for x in idData:
        products = Products.objects.get(id=x['productid'])
        productData = ProductsSerializer(products)
        x['name'] = productData.data['name']
        x['company_name'] = productData.data['company_name']
        for y in productData.data['batch']:
            if x['batchid'] == y['id']:
                x['productdata'] = y
        finaldata.append(x)
    return Response(finaldata)


@ api_view(['POST', 'GET'])
def returnlist(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    returns = PurchaseReturn.objects.filter(retailerid=loggedRetailer)
    serializer = PurchaseReturnSerializer(returns, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@ api_view(['POST', 'GET'])
def openchallan(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    vendor_id = request.data.get('vendor_id')
    purchases = Purchase.objects.filter(vendorid=vendor_id).filter(
        purchase_type='Challan').filter(converted=0).filter(retailerid=loggedRetailer)
    serializer = PurchaseSerializer(purchases, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@ api_view(['POST', 'GET'])
def batchDetails(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    finaldata = []
    batch_id = request.data.get('id')
    Purchasedata = Purchase.objects.filter(
        retailerid=loggedRetailer).filter(purchase__batchid=batch_id)[:1]
    serializer = PurchaseSerializer(Purchasedata,many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)



@ api_view(['POST', 'GET'])
def batchDetails1(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    batch_id = request.data.get('id')
    try:
        purchases = PurchaseDetail.objects.filter(
            retailerid=loggedRetailer).filter(batchid=batch_id)
    except PurchaseDetail.DoesNotExist:
        return HttpResponse('LIP')
    p=0
    for x in purchases:
        if p == 0:
            thisDetailId = x['id']
            thisInstance= PurchaseDetail.objects.get(id=thisDetailId)
            serializer = PurchaseDetailSerializer(thisInstance)
            purchase_id = serializer.data['purchaseid']
            purchaseData = Purchase.objects.filter(retailerid=loggedRetailer).filter(
                purchase_type='Bill').get(id=purchase_id)
            serializer = PurchaseSerializer(purchaseData)
            return Response(serializer.data, status=status.HTTP_200_OK)


@ api_view(['POST', 'GET'])
def saveReturn(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    PurchaseReturnDetail.objects.filter(draft=1).delete()
    if request.data:
        medicineData = request.data.getlist('medicines')
        for x in medicineData:
            x = json.loads(x)
            for y in x:
                productConversionQuery = Products.objects.get(
                    id=y['productid'])
                conversion_ratio = productConversionQuery.conversion_ratio
                stockvalue = (float(y['qty']) * float(conversion_ratio))

                thisData = {'productid': y['productid'], 'batchid': y['batch'], 'mrp': y['mrp'], 'quantity': y['qty'],
                            'vendorid': y['sellerid'], 'subtotal': y['subtotal'], 'draft': 1, 'billno': y['billno'],'retailerid':loggedRetailer,'stockvalue':stockvalue}
                purchasedetail_Serializer = PurchaseReturnDetailSerializer(data=thisData)
                if purchasedetail_Serializer.is_valid():
                    purchasedetail_Serializer.save()
    return HttpResponse('Done')


@ api_view(['POST', 'GET'])
def purchaseReturnListDraft(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    draft = 1
    purchaseData = PurchaseReturnDetail.objects.filter(draft=draft).filter(retailerid=loggedRetailer)
    serializer = PurchaseReturnDetailSerializer(purchaseData, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@ api_view(['POST', 'GET'])
def finalSaveReturn(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    lastvendorid = 0
    allDrafts = PurchaseReturnDetail.objects.filter(
        draft=1).filter(retailerid=loggedRetailer).order_by('-vendorid')
    for x in allDrafts:
        thisVendorid = x.vendorid
        if thisVendorid != lastvendorid:
            lastvendorid = thisVendorid
            thisReturnData = {
                'vendorid':thisVendorid,
                'retailerid' : loggedRetailer
            }
            thisReturnInstance = PurchaseReturnSerializer(data=thisReturnData)
            if thisReturnInstance.is_valid():
                thisRetIdIns = thisReturnInstance.save()
                thisReturnId = thisRetIdIns.id
                thisVendorDrafts = PurchaseReturnDetail.objects.filter(draft=1).filter(retailerid=loggedRetailer).filter(vendorid=thisVendorid).update(purchasereturnid=thisReturnId)
                thisSubtotal = PurchaseReturnDetail.objects.filter(purchasereturnid=thisReturnId).aggregate(Sum('subtotal'))
                thisSubtotal_tosave = thisSubtotal['subtotal__sum']
                PurchaseReturn.objects.filter(id=thisReturnId).update(subtotal=thisSubtotal_tosave)
    toUpdateData = []
    pd_list = PurchaseReturnDetail.objects.filter(draft=1).filter(retailerid=loggedRetailer)
    for x in pd_list:
        toUpdateData.append(x)
    PurchaseReturnDetail.objects.all().update(draft=0)
    for x in toUpdateData:
        thisProductId = x.productid
        thisBatchId = x.batchid
        productStockBatch(thisProductId, loggedRetailer, thisBatchId)
        productStock(thisProductId, loggedRetailer)
    return HttpResponse('Done')


def addStockValue(productid,qty,qtyfree,purchasedetailsid):
    thisConversion = Products.objects.get(id=productid)
    ratio = thisConversion.conversion_ratio
    stockvalue = (float(ratio) * (float(qty) + float(qtyfree)))
    thisPurchase = PurchaseDetail.objects.get(id=purchasedetailsid)
    thisPurchase.stockvalue = stockvalue
    thisPurchase.save()
    return "OK"


@ api_view(['POST', 'GET'])
def challanDetails(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    finaldata = []
    purchaseid  = request.data.get('purchaseid')
    purchase = PurchaseDetail.objects.filter(challanid=purchaseid)
    serializer = PurchaseDetailSerializer(purchase, many=True)
    for x in serializer.data:
        productid = x['productid']
        products = Products.objects.filter(id=productid).filter(retailerproductmapping__retailerid=loggedRetailer).filter(batch__retailerid=loggedRetailer)
        products = products.distinct()
        thisPdata = ProductsSerializer(products, many=True, context={"loggedRetailer": loggedRetailer})
        x['productdata'] =  thisPdata.data
        finaldata.append(x)
    return Response(finaldata, status=status.HTTP_200_OK)


@ api_view(['POST', 'GET'])
def addPayment(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    amount = float(request.data.get('amount'))
    purchaseid = request.data.get('invoiceid')
    purchasedata = Purchase.objects.get(id=purchaseid)
    vendorid = purchasedata.vendorid_id
    payment_made = purchasedata.payment_made
    if payment_made == "":
        payment_made = 0
    payment_made = float(payment_made)+amount
    purchasedata.payment_made = payment_made
    purchasedata.save()
    ledgertypes = []
    ledgerids = []
    debitamounts = []
    creditamounts = []
    notes = []
    entrytypes = []
    entryids = []
    ledgertypeids = []
    thisLedgerObject = Ledger.objects.filter(
        ledgertype='Supplier').get(ledgertypeid=vendorid)
    vtype = "Purchase"
    vsubtype = "Payment"
    vtitle = "Payment Issue"
    debitamounts.append(amount)
    creditamounts.append(0)
    ledgertypes.append('Supplier')
    ledgertypeids.append(vendorid)
    notes.append('Payment Against: PR-'+purchaseid)
    entrytypes.append('Purchase')
    entryids.append(purchaseid)
    ledgerids.append(thisLedgerObject.id)

    debitamounts.append(0)
    creditamounts.append(amount)
    ledgertypes.append('Cash')
    ledgertypeids.append('')
    notes.append('')
    entrytypes.append('Purchase Return')
    entryids.append(purchaseid)
    ledgerids.append(8)

    

    t = saveVoucher(vtype, vsubtype, vtitle, ledgertypes,
                    ledgerids, debitamounts, creditamounts, loggedRetailer, notes, entrytypes, entryids, ledgertypeids)
    fixLedgerBalance(thisLedgerObject.id, 'Supplier', vendorid)
    return HttpResponse(t)


@ api_view(['POST', 'GET'])
def addCreditNote(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    amount = float(request.data.get('amount'))
    creditnumber = request.data.get('creditnumber')
    purchaseid = request.data.get('invoiceid')
    purchasedata = PurchaseReturn.objects.get(id=purchaseid)
    vendorid = purchasedata.vendorid_id
    purchasedata.creditnumber = creditnumber
    purchasedata.creditvalue = amount
    purchasedata.save()
    ledgertypes = []
    ledgerids = []
    debitamounts = []
    creditamounts = []
    notes = []
    entrytypes = []
    entryids = []
    ledgertypeids = []
    thisLedgerObject = Ledger.objects.filter(
        ledgertype='Supplier').get(ledgertypeid=vendorid)
    vtype = "Purchase Return (Debit Note)"
    vsubtype = "Purchase Return"
    vtitle = "Purchase Return"
    debitamounts.append(amount)
    creditamounts.append(0)
    ledgertypes.append('Supplier')
    ledgertypeids.append(vendorid)
    notes.append('Payment against: PR-'+purchaseid)
    entrytypes.append('Purchase')
    entryids.append(purchaseid)
    ledgerids.append(thisLedgerObject.id)

    debitamounts.append(0)
    creditamounts.append(amount)
    ledgertypes.append('Cash')
    ledgertypeids.append('')
    notes.append('')
    entrytypes.append('Purchase')
    entryids.append(purchaseid)
    ledgerids.append(8)


    total_invoice_value = amount
    debitamounts.append(0)
    creditamounts.append(total_invoice_value)
    ledgertypes.append('GST LOCAL SALES')
    ledgertypeids.append('')
    notes.append('')
    entrytypes.append('Purchase Return')
    entryids.append(purchaseid)
    ledgerids.append(13)

    taxvalue = total_invoice_value - total_invoice_value/1.05
    taxvalue = taxvalue/2
    taxvalue = round(taxvalue)

    debitamounts.append(0)
    creditamounts.append(taxvalue)
    ledgertypes.append('CGST SALES TAX')
    ledgertypeids.append('')
    notes.append('')
    entrytypes.append('Purchase Return')
    entryids.append(purchaseid)
    ledgerids.append(14)

    debitamounts.append(0)
    creditamounts.append(taxvalue)
    ledgertypes.append('SGST SALES TAX')
    ledgertypeids.append('')
    notes.append('')
    entrytypes.append('Purchase Return')
    entryids.append(purchaseid)
    ledgerids.append(15)


    t = saveVoucher(vtype, vsubtype, vtitle, ledgertypes,
                    ledgerids, debitamounts, creditamounts, loggedRetailer, notes, entrytypes, entryids, ledgertypeids)
    fixLedgerBalance(thisLedgerObject.id, 'Supplier', vendorid)





    return HttpResponse('Done')
