from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'vendors'
urlpatterns = [
    path('list/', views.list,
         name='list'),
    path('detailsviamobile/', views.detailsviamobile,
         name='detailsviamobile'),
    path('save/', views.save,
         name='save'),

]
