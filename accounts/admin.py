from django.contrib import admin

from .models import Voucher, Ledger, AccountGroup, VoucherDetail, Settings

admin.site.register(Voucher)


class AccountGroupAdmin(admin.ModelAdmin):
    model = AccountGroup
    list_display = ['id', 'accountname']


admin.site.register(AccountGroup, AccountGroupAdmin)


class SettingsAdmin(admin.ModelAdmin):
    model = Settings
    list_display = ['subject', 'value', 'inputtype', 'displayname']


admin.site.register(Settings, SettingsAdmin)



class VoucherAdmin(admin.ModelAdmin):
    model = VoucherDetail
    list_display = ['ledgertype', 'debit',
                    'credit', 'notes']


admin.site.register(VoucherDetail, VoucherAdmin)


class LedgerAdmin(admin.ModelAdmin):
    model = Ledger
    list_display = ['name', 'ledgertype',
                    'ledgertypeid', 'debit', 'credit', 'balance']


admin.site.register(Ledger, LedgerAdmin)

# Register your models here.
