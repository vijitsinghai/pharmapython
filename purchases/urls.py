from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'purchases'
urlpatterns = [
    path('save/', views.save,
         name='save'),
    path('list/', views.getlist,
         name='list'),
    path('quickview/', views.quickview,
         name='quickview'),
    path('open_challan/', views.openchallan,
         name='open_challan'),
    path('batch_details/', views.batchDetails,
         name='batch_details'),
    path('save_return/', views.saveReturn,
         name='save_return'),
    path('return-drafts/', views.purchaseReturnListDraft,
         name='return-drafts'),
    path('finalSaveReturn/', views.finalSaveReturn,
         name='finalSaveReturn'),
    path('challan-details/', views.challanDetails,
         name='challanDetails'),
    path('returnlist/', views.returnlist,
         name='returnlist'),
    path('add_payment/', views.addPayment,
         name='add_payment'),
    path('add_creditnote/', views.addCreditNote,
         name='add_creditnote')

         

         

]
