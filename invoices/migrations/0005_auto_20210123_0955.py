# Generated by Django 3.1.3 on 2021-01-23 09:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0036_auto_20210123_0834'),
        ('invoices', '0004_auto_20210109_0841'),
    ]

    operations = [
        migrations.RenameField(
            model_name='salesorder',
            old_name='mrp',
            new_name='premayment',
        ),
        migrations.RemoveField(
            model_name='salesorder',
            name='invoiceid',
        ),
        migrations.RemoveField(
            model_name='salesorder',
            name='primary_unit',
        ),
        migrations.RemoveField(
            model_name='salesorder',
            name='productid',
        ),
        migrations.RemoveField(
            model_name='salesorder',
            name='secondary_unit',
        ),
        migrations.CreateModel(
            name='SalesOrderDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mrp', models.CharField(blank=True, max_length=200)),
                ('primary_unit', models.CharField(blank=True, max_length=200)),
                ('secondary_unit', models.CharField(blank=True, max_length=200)),
                ('createdate', models.DateTimeField(auto_now_add=True)),
                ('productid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='salesorder_product', to='products.products')),
                ('salesorderid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='salesorder_id', to='invoices.salesorder')),
            ],
        ),
    ]
