import json
from django.http import JsonResponse
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Products, Substitute, Salt, ProductCompany, Unit, RetailerProductMapping, Batch
from .serializers import ProductsSerializer, ProductsFilterSerializer, BatchSerializer, SubstituteSerializer, SaltSerializer, CompanySerializer, UnitSerializer, SaltMappingSerializer
from rest_framework import status
from purchases.models import PurchaseDetail, PurchaseReturnDetail
from invoices.models import InvoiceDetail
from django.db.models import Sum
from retailers.views import checkAuth, loggedRetailer

# Create your views here.
loggedRetailer = "0"


@api_view(['GET', 'POST'])
def datalist(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    request.session['loggedRetailer'] = loggedRetailer
    if loggedRetailer == "0":
        return Response('Invalid Login')
    products = Products.objects.all().order_by('name')
    serializer = ProductsSerializer(products, many=True)
    return Response(serializer.data)


@api_view(['GET', 'POST'])
def saltlist(request):
    salts = Salt.objects.all()
    serializer = SaltSerializer(salts, many=True)
    return Response(serializer.data)


@api_view(['GET', 'POST'])
def companies(request):
    companies = ProductCompany.objects.all()
    serializer = CompanySerializer(companies, many=True)
    return Response(serializer.data)


@api_view(['GET', 'POST'])
def units(request):
    units = Unit.objects.all()
    serializer = UnitSerializer(units, many=True)
    return Response(serializer.data)


@api_view(['GET', 'POST'])
def suggestions(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    products = Products.objects.filter(
        name__startswith=request.data.get('productName')).filter(retailerproductmapping__retailerid=loggedRetailer).filter(batch__retailerid=loggedRetailer)
    products = products.distinct()
    serializer = ProductsSerializer(products, many=True, context={
                                    "loggedRetailer": loggedRetailer})
    return Response(serializer.data)


@api_view(['GET', 'POST'])
def nameSuggestions(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    products = Products.objects.filter(
        name__startswith=request.data.get('productName')).filter(retailerproductmapping__retailerid=loggedRetailer)
    products = products.distinct()
    return JsonResponse(products,safe=False)




@api_view(['GET', 'POST'])
def getSubstitutes(request):
    finaldata = []
    substitutes = Substitute.objects.filter(
        productid__in=request.data.getlist('id[]'))
    for x in substitutes:
        products = Products.objects.filter(id=x.substituteid)
        serializer = ProductsSerializer(products, many=True)
        finaldata.append(serializer.data)
    return Response(finaldata)


@ api_view(['POST'])
def savedata(request):
    loggedRetailer = checkAuth(request.data.get('token'))
    if loggedRetailer == "0":
        return Response('Invalid Login')
    data = request.data.getlist('data[]')
    current_stock = int(data[10]) * int(data[9])
    customerdata = {
        'name': data[0],
        'company': int(data[1]),
        'unit': int(data[2]),
        'hsn': data[3],
        'primary_unit': data[4],
        'secondary_unit': data[5],
        'narcotic': int(data[7]),
        'tax': data[8],
        'opening_stock': current_stock,
        'current_stock': current_stock,
        'conversion_ratio': data[10],
        'packaging': data[11],
        'rack': data[12]
    }
   # return JsonResponse(customerdata, safe=False)
    serializer = ProductsSerializer(data=customerdata)
    if serializer.is_valid():
        sInstance = serializer.save()
        productid = sInstance.id
        salts = data[6]
        salts = json.loads(salts)
        for x in salts:
            x = x.replace("A_", "")
            thisdata = {
                'saltid': int(x),
                'productid': productid,
                'salt_id': int(x),
                'product_id': productid
            }
            saltSerz = SaltMappingSerializer(data=thisdata)
            if saltSerz.is_valid():
                saltSerz.save()
                return Response(saltSerz.data, status=status.HTTP_201_CREATED)
            return Response(saltSerz.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def productStock(productid, loggedRetailer):
    totalStock = Batch.objects.filter(
        productid=productid).filter(retailerid=loggedRetailer).aggregate(Sum('currentstock'))
    if totalStock['currentstock__sum'] is None:
        totalStock['currentstock__sum'] = 0
    mapping = RetailerProductMapping.objects.filter(
        productid=productid).get(retailerid=loggedRetailer)
    mapping.currentstock = totalStock['currentstock__sum']
    mapping.save()
    return totalStock['currentstock__sum']


def productStockBatch(productid, loggedRetailer, batchid):
    openingStock = Batch.objects.filter(
        productid=productid).filter(retailerid=loggedRetailer).filter(id=batchid).aggregate(Sum('openingstock'))
    totalPurchased = PurchaseDetail.objects.filter(productid=productid).filter(
        batchid=batchid).filter(retailerid=loggedRetailer).aggregate(Sum('stockvalue'))
    totalSold = InvoiceDetail.objects.filter(
        productid=productid).filter(batchid=batchid).filter(retailerid=loggedRetailer).aggregate(Sum('stockvalue'))
    totalReturned = PurchaseReturnDetail.objects.filter(productid=productid).filter(
        batchid=batchid).filter(retailerid=loggedRetailer).filter(draft=0).aggregate(Sum('stockvalue'))

    if openingStock['openingstock__sum'] is None:
        openingStock['openingstock__sum'] = 0
    if totalPurchased['stockvalue__sum'] is None:
        totalPurchased['stockvalue__sum'] = 0
    if totalSold['stockvalue__sum'] is None:
        totalSold['stockvalue__sum'] = 0
    if totalReturned['stockvalue__sum'] is None:
        totalReturned['stockvalue__sum'] = 0

    totalStock = float(openingStock['openingstock__sum']) + float(
        totalPurchased['stockvalue__sum']) - float(totalSold['stockvalue__sum']) - float(totalReturned['stockvalue__sum'])

    mapping = Batch.objects.get(id=batchid)
    mapping.currentstock = totalStock
    mapping.save()
    return totalStock

# @api_view(['POST'])
# def savedata(request):
#    data = {'productName': request.data.get('productName')}
#    serializer = ProductsSerializer(data=data)
#    if serializer.is_valid():
#        t = serializer.save()
#        pid = t.id
#        data = {'number': request.data.get(
#            'number'), 'expiry': request.data.get('expiry'), 'productid': pid}
#        batchSaving = BatchSerializer(data=data)
#        if batchSaving.is_valid():
#            batchSaving.save()
#            return Response(batchSaving.data, status=status.HTTP_201_CREATED)
#        return Response(batchSaving.errors, status=status.HTTP_400_BAD_REQUEST)
#    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
