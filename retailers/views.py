from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Retailer, AuthToken
from rest_framework import status


@api_view(['GET', 'POST'])
def list(request):
    Retailer = Retailer.objects.all().order_by('name')
#    serializer = VendorSerializer(vendors, many=True)
 #   return Response(serializer.data)
    return HttpResponse('abcd')


loggedRetailer = 0

def checkAuth(token):
    retailers = AuthToken.objects.filter(token=token)
    if retailers.count() > 0:
        loggedRetailer = retailers[0].id
        return  retailers[0].id
    else:
        return "0"

def currentLoggedUser():
    return loggedRetailer
