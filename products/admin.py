from django.contrib import admin

from .models import Products, Batch, Substitute, Salt, ProductCompany, Unit, SaltProductMapping, RetailerProductMapping

admin.site.register(Products)
admin.site.register(Batch)
admin.site.register(Substitute)
admin.site.register(Salt)
admin.site.register(ProductCompany)
admin.site.register(Unit)
admin.site.register(SaltProductMapping)
admin.site.register(RetailerProductMapping)
# Register your models here.
