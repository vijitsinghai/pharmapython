from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'retailers'
urlpatterns = [
    path('list/', views.list,
         name='list'),
    path('checkAuth', views.checkAuth, name="checkAuth")

]
