from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'doctor'
urlpatterns = [
    path('list/', views.list,
         name='list'),
    path('save/', views.save,
         name='save')

]
