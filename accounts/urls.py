from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'accounts'
urlpatterns = [
    path('accountgroups/', views.accountgroups,
         name='accountgroups'),
    path('children/', views.childList,
         name='childList'),
    path('ledgerlist/', views.ledgerList,
         name='ledgerList'),
    path('voucherlist/', views.voucherlist, name="voucherlist"),
    path('voucherdetails/', views.voucherdetails, name="voucherdetails")
]
