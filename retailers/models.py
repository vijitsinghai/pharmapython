from django.db import models
# Create your models here.


class Retailer(models.Model):
    name = models.CharField(max_length=200)
    mobile = models.CharField(max_length=200, null=False,
                              blank=True)
    email = models.CharField(max_length=200, null=False,
                             blank=True)
    addresstitle = models.CharField(max_length=200, null=False,
                                    blank=True)
    address = models.CharField(max_length=200, null=False,
                               blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)

    def __int__(self):
        return self.id


class AuthToken(models.Model):
    retailerid = models.ForeignKey(
        Retailer, on_delete=models.CASCADE)
    token = models.CharField(max_length=200, null=False,
                               blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)

    def __int__(self):
        return self.retailerid
