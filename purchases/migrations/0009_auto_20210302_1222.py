# Generated by Django 3.1.4 on 2021-03-02 12:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('retailers', '0001_initial'),
        ('purchases', '0008_purchasereturn_billno'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchase',
            name='retailerid',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='retailers.retailer'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='purchasedetail',
            name='retailerid',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='retailers.retailer'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='purchasereturn',
            name='retailerid',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='retailers.retailer'),
            preserve_default=False,
        ),
    ]
