from django.db import models
from customers.models import Customer
from products.models import Products, Batch
from retailers.models import Retailer
# Create your models here.


class Invoice(models.Model):
    customerid = models.ForeignKey(
        Customer, related_name='invoice_customer', on_delete=models.CASCADE, default="1")
    invoice_number = models.CharField(max_length=200, null=False,
                                      blank=True)
    subtotal = models.CharField(max_length=200, null=False,
                                blank=True)
    discount = models.CharField(max_length=200, null=False,
                                blank=True)
    delivery_charge = models.CharField(max_length=200, null=False,
                                       blank=True)
    payment_due = models.CharField(max_length=200, null=False,
                                   blank=True)
    previous_credit = models.CharField(max_length=200, null=False,
                                       blank=True)
    outstanding = models.CharField(max_length=200, null=False,
                                   blank=True)
    total_invoice_value = models.CharField(max_length=200, null=False,
                                           blank=True)
    invoice_date = models.DateTimeField(null=False,
                                        blank=False, auto_now_add=True)
    prescription_days = models.CharField(max_length=200, null=False,
                                         blank=True)
    payment_made = models.CharField(max_length=200, null=True,
                                    blank=True)
    payment_made_cash = models.CharField(max_length=200, null=True,
                                         blank=True)
    payment_made_bank = models.CharField(max_length=200, null=True,
                                    blank=True)
    voucherid = models.CharField(max_length=200, null=True,
                                    blank=True)
    vouchernumber = models.CharField(max_length=200, null=True,
                                    blank=True)

    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer, on_delete=models.CASCADE)

    def __int__(self):
        return self.id


class InvoiceDetail(models.Model):
    invoiceid = models.ForeignKey(
        Invoice, related_name='invoice', on_delete=models.CASCADE)
    productid = models.ForeignKey(
        Products, related_name='invoice_product', on_delete=models.CASCADE)
    batchid = models.ForeignKey(
        Batch, related_name='invoice_batch', on_delete=models.CASCADE)
    mrp = models.CharField(max_length=200, null=False,
                           blank=True)
    primary_unit = models.CharField(max_length=200, null=False,
                                    blank=True)
    secondary_unit = models.CharField(max_length=200, null=False,
                                      blank=True)
    discount = models.CharField(max_length=200, null=False,
                                blank=True)
    subtotal = models.CharField(max_length=200, null=False,
                                blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    stockvalue = models.CharField(max_length=200, null=False,
                                blank=True)
    retailerid = models.ForeignKey(
        Retailer,  on_delete=models.CASCADE)

    def __int__(self):
        return self.id


class SalesOrder(models.Model):
    customerid = models.ForeignKey(
        Customer, related_name='salesorder_customer', on_delete=models.CASCADE)
    prepayment = models.CharField(max_length=200, null=False,
                                  blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer,  on_delete=models.CASCADE)

    def __int__(self):
        return self.id


class SalesOrderDetails(models.Model):
    customerid = models.ForeignKey(
        Customer, related_name='salesorderdetails_customer', on_delete=models.CASCADE)
    salesorderid = models.ForeignKey(
        SalesOrder, related_name='salesorder_id', on_delete=models.CASCADE)
    productid = models.ForeignKey(
        Products, related_name='salesorder_product', on_delete=models.CASCADE)
    mrp = models.CharField(max_length=200, null=False,
                           blank=True)
    primary_unit = models.CharField(max_length=200, null=False,
                                    blank=True)
    secondary_unit = models.CharField(max_length=200, null=False,
                                      blank=True)
    total_units = models.IntegerField(max_length=200, null=False,
                                      blank=True)
    total_units_delivered = models.IntegerField(max_length=200, null=False,
                                                blank=True)
    associated_invoice = models.IntegerField(max_length=200, null=True,
                                             blank=True)
    createdate = models.DateTimeField(null=False,
                                      blank=False, auto_now_add=True)
    retailerid = models.ForeignKey(
        Retailer, on_delete=models.CASCADE)

    def __int__(self):
        return self.id
