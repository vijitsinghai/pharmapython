from django.contrib import admin

from .models import Invoice, InvoiceDetail, SalesOrder, SalesOrderDetails
from products.models import Products


class InvoiceAdmin(admin.ModelAdmin):
    model = Invoice
    list_display = ['invoice_date', 'customername',
                    'customermobile', 'total_invoice_value']

    def customername(self, obj):
        return obj.customerid.name

    def customermobile(self, obj):
        return obj.customerid.mobile


admin.site.register(Invoice, InvoiceAdmin)


class InvoiceDetailAdmin(admin.ModelAdmin):
    model = InvoiceDetail
    list_display = ['productname', 'batchnumber', 'mrp','stockvalue']

    def productname(self, obj):
        return obj.productid.name

    def batchnumber(self, obj):
        return obj.batchid.number

    def stockvalue(self, obj):
        return obj.stockvalue


admin.site.register(InvoiceDetail, InvoiceDetailAdmin)
admin.site.register(SalesOrder)
admin.site.register(SalesOrderDetails)
# Register your models here.
