# Generated by Django 3.1.4 on 2021-01-12 06:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0014_auto_20210112_0641'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='salt',
            field=models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, related_name='salt', to='products.salt'),
        ),
    ]
